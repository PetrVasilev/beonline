import React from 'react'
import styled from 'styled-components'

const View = styled.div`
    display: flex;
    flex-direction: row;
    align-items: flex-start;
    justify-content: flex-start;
    font-size: 14px;
    max-width: 600px;
    margin-bottom: 12px;
    line-height: 20px;

    .label {
        font-weight: 300;
        width: 30%;
        margin-right: 15px;
    }

    .value {
        width: 70%;
        white-space: pre-line;
    }

    @media only screen and (max-width: 600px) {
        flex-direction: column;

        .label {
            margin-right: 0;
            margin-bottom: 3px;
            width: 100%;
        }

        .value {
            width: 100%;
        }
    }

    .help {
        position: absolute;
    }
`

const LabeledValue = ({ label, value, className, valueClassName }) => {
    return (
        <View className={className}>
            <div className="label">{label}</div>
            <div className={`value${valueClassName ? ` ${valueClassName}` : ''}`}>
                {value ? value : '-'}
            </div>
        </View>
    )
}

export default LabeledValue
