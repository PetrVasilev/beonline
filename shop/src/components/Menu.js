import React from 'react'
import styled from 'styled-components'
import { Link } from 'react-router-dom'
import { Menu } from 'antd'
import {
    UserOutlined,
    OrderedListOutlined,
    ShoppingCartOutlined,
    ShoppingOutlined
} from '@ant-design/icons'

const MenuComponent = () => {
    const { pathname } = window.location
    return (
        <Menu theme="dark" mode="inline" defaultSelectedKeys={[pathname]}>
            <Menu.Item style={{ marginTop: 7 }} key="/">
                <MenuLink to="/">
                    <UserOutlined />
                    Личный кабинет
                </MenuLink>
            </Menu.Item>
            <Menu.Item key="/categories">
                <MenuLink to="/categories">
                    <OrderedListOutlined />
                    Категории
                </MenuLink>
            </Menu.Item>
            <Menu.Item key="/products">
                <MenuLink to="/products">
                    <ShoppingCartOutlined />
                    Товары
                </MenuLink>
            </Menu.Item>
            <Menu.Item key="/orders">
                <MenuLink to="/orders">
                    <ShoppingOutlined />
                    Заказы
                </MenuLink>
            </Menu.Item>
        </Menu>
    )
}

const MenuLink = styled(Link)`
    display: flex;
    flex-direction: row;
    align-items: center;

    a {
        color: white;
    }
`

export default MenuComponent
