import gql from 'graphql-tag'

export const GET_ORDERS = gql`
    query($status: Int, $id: Int, $skip: Int, $first: Int, $search: String) {
        orders(where: { status: $status, id: $id }, skip: $skip, first: $first, search: $search) {
            id
            userName
            userPhone
            userEmail
            userAddress
            createdAt
            status
            cartItems {
                id
                count
                product {
                    id
                    name
                    description
                    price
                }
            }
        }

        ordersCount(where: { status: $status, id: $id }, search: $search)
    }
`

export const GET_ORDER = gql`
    query($id: Int!) {
        order(where: { id: $id }) {
            id
            userName
            userPhone
            userEmail
            userAddress
            createdAt
            status
            cartItems {
                id
                count
                product {
                    id
                    name
                    description
                    price
                }
            }
        }
    }
`

export const SET_STATUS_ORDER = gql`
    mutation($id: Int!, $status: Int!) {
        setStatusOrder(where: { id: $id }, data: { status: $status }) {
            id
            userName
            userPhone
            userEmail
            userAddress
            createdAt
            status
            cartItems {
                id
                count
                product {
                    id
                    name
                    description
                    price
                }
            }
        }
    }
`

export const DELETE_ORDER = gql`
    mutation($id: Int!) {
        deleteOrder(where: { id: $id }) {
            id
            userName
            userPhone
            userEmail
            userAddress
            createdAt
            status
            cartItems {
                id
                count
                product {
                    id
                    name
                    description
                    price
                }
            }
        }
    }
`
