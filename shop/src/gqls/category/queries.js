import gql from 'graphql-tag'

export const GET_CATEGORIES = gql`
    query($shopId: Int!) {
        categories(where: { shopId: $shopId }) {
            id
            name
            subcategories {
                id
                name
                sortNumber
            }
        }
    }
`

export const GET_CATEGORY = gql`
    query($categoryId: Int!) {
        category(where: { id: $categoryId }) {
            id
            name
            subcategories {
                id
                name
                sortNumber
            }
        }
    }
`
