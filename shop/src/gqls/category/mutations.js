import gql from 'graphql-tag'

export const CREATE_CATEGORY = gql`
    mutation($name: String!) {
        createCategory(data: { name: $name }) {
            id
            name
            subcategories {
                id
                name
            }
        }
    }
`

export const DELETE_CATEGORY = gql`
    mutation($id: Int!) {
        deleteCategory(where: { id: $id }) {
            id
            name
            subcategories {
                id
                name
            }
        }
    }
`

export const CREATE_SUBCATEGORY = gql`
    mutation($name: String!, $categoryId: Int!) {
        createSubcategory(where: { id: $categoryId }, data: { name: $name }) {
            id
            name
        }
    }
`

export const DELETE_SUBCATEGORY = gql`
    mutation($id: Int!) {
        deleteSubcategory(where: { id: $id }) {
            id
            name
        }
    }
`

export const SET_SORT_CATEGORY = gql`
    mutation($arr: [SortCategoryInput!]!) {
        setSortCategory(data: $arr)
    }
`

export const SET_SORT_SUBCATEGORY = gql`
    mutation($arr: [SortSubcategoryInput!]!) {
        setSortSubcategory(data: $arr)
    }
`
