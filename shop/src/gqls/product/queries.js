import gql from 'graphql-tag'

export const GET_PRODUCTS = gql`
    query($shopId: Int!, $categoryId: Int, $subcategoryId: Int, $archive: Boolean) {
        products(
            where: {
                shopId: $shopId
                categoryId: $categoryId
                subcategoryId: $subcategoryId
                archive: $archive
            }
        ) {
            id
            name
            price
            description
            images
            createdAt
            archive
            category {
                id
                name
            }
            subcategory {
                id
                name
            }
        }
    }
`

export const GET_PRODUCT = gql`
    query($id: Int!) {
        product(where: { id: $id }) {
            id
            name
            price
            description
            images
            createdAt
            archive
            category {
                id
                name
            }
            subcategory {
                id
                name
            }
        }
    }
`
