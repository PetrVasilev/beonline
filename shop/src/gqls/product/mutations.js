import gql from 'graphql-tag'

export const CREATE_PRODUCT = gql`
    mutation(
        $name: String!
        $price: Float!
        $description: String
        $images: [Upload]
        $categoryId: Int
        $subcategoryId: Int
    ) {
        createProduct(
            data: {
                name: $name
                price: $price
                description: $description
                images: $images
                categoryId: $categoryId
                subcategoryId: $subcategoryId
            }
        ) {
            id
            name
            price
            description
            images
            createdAt
            archive
            category {
                id
                name
            }
            subcategory {
                id
                name
            }
        }
    }
`

export const EDIT_PRODUCT = gql`
    mutation(
        $name: String!
        $price: Float!
        $description: String
        $images: [Upload]
        $categoryId: Int
        $subcategoryId: Int
        $deletedImages: [String]
        $id: Int!
        $archive: Boolean
    ) {
        editProduct(
            data: {
                name: $name
                price: $price
                description: $description
                images: $images
                categoryId: $categoryId
                subcategoryId: $subcategoryId
                deletedImages: $deletedImages
                archive: $archive
            }
            where: { id: $id }
        ) {
            id
            name
            price
            description
            images
            createdAt
            archive
            category {
                id
                name
            }
            subcategory {
                id
                name
            }
        }
    }
`

export const DELETE_PRODUCT = gql`
    mutation($id: Int!) {
        deleteProduct(where: { id: $id }) {
            id
            name
            price
            description
            images
            createdAt
            archive
            category {
                id
                name
            }
            subcategory {
                id
                name
            }
        }
    }
`

export const SET_SORT_PRODUCT = gql`
    mutation($arr: [SortProductInput!]!) {
        setSortProduct(data: $arr)
    }
`
