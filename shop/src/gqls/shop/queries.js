import gql from 'graphql-tag'

export const GET_SHOP = gql`
    {
        shop {
            id
            email
            name
            createdAt
            description
            contactsForAdmin
            contactsForUsers
            contactsWhatsapp
            logo
            access
            url
            delivery
        }
    }
`
