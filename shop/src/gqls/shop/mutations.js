import gql from 'graphql-tag'

export const LOGIN_SHOP = gql`
    mutation($email: String!, $password: String!) {
        loginShop(data: { email: $email, password: $password }) {
            token
            shop {
                id
                email
                name
                createdAt
                description
                contactsForAdmin
                contactsForUsers
                contactsWhatsapp
                logo
                access
                url
                delivery
            }
        }
    }
`

export const EDIT_PASSWORD = gql`
    mutation($currentPassword: String!, $newPassword: String!) {
        editPasswordShop(data: { currentPassword: $currentPassword, newPassword: $newPassword }) {
            id
            email
            name
            createdAt
            description
            contactsForAdmin
            contactsForUsers
            contactsWhatsapp
            logo
            access
            url
            delivery
        }
    }
`

export const EDIT_SHOP = gql`
    mutation(
        $id: Int!
        $name: String!
        $description: String
        $contactsForUsers: String
        $contactsWhatsapp: String
        $logo: Upload
        $url: String
        $delivery: Boolean
    ) {
        editShop(
            where: { id: $id }
            data: {
                name: $name
                description: $description
                contactsForUsers: $contactsForUsers
                contactsWhatsapp: $contactsWhatsapp
                logo: $logo
                url: $url
                delivery: $delivery
            }
        ) {
            id
            email
            name
            createdAt
            description
            contactsForAdmin
            contactsForUsers
            contactsWhatsapp
            logo
            access
            url
            delivery
        }
    }
`
