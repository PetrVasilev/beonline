// utils here
import React from 'react'
import { Tag } from 'antd'

export const getAccessView = (access) => {
    switch (access) {
        case 0:
            return (
                <Tag style={{ minWidth: 90, textAlign: 'center' }} color="red">
                    Нет доступа
                </Tag>
            )
        case 1:
            return (
                <Tag style={{ minWidth: 90, textAlign: 'center' }} color="green">
                    Есть доступ
                </Tag>
            )
        default:
            return null
    }
}

export const getOrderStatusView = (status) => {
    switch (status) {
        case 0:
            return <Tag color="blue">Новый</Tag>
        case 1:
            return <Tag>В процессе</Tag>
        case 2:
            return <Tag color="green">Выполнен</Tag>
        case 3:
            return <Tag color="red">Отменен</Tag>
        default:
            return null
    }
}

export const DOMAIN =
    process.env.NODE_ENV === 'production' ? 'https://beonline.me' : 'http://localhost:3001'

export const convertPrice = (price) => {
    if (price % 1 === 0) {
        return price
    } else {
        return price.toFixed(2)
    }
}
