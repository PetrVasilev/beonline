import React from 'react'
import ReactDOM from 'react-dom'
import { BrowserRouter as Router, Switch, Route } from 'react-router-dom'
import { createGlobalStyle } from 'styled-components'
import { ConfigProvider } from 'antd'
import { ApolloProvider } from '@apollo/react-hooks'
import ru from 'antd/es/locale/ru_RU'
import 'antd/dist/antd.css'
import 'moment/locale/ru'

import * as serviceWorker from './serviceWorker'
import withLayout from './components/Layout'
import apollo from './utils/apollo'

import Profile from './containers/profile'
import Password from './containers/profile/password'
import EditProfile from './containers/profile/edit'
import Login from './containers/login'
import Categories from './containers/categories'
import Subcategories from './containers/categories/subcategories'
import AddCategory from './containers/categories/add'
import Products from './containers/products'
import AddProduct from './containers/products/add'
import SingleProduct from './containers/products/single'
import EditProduct from './containers/products/edit'
import Orders from './containers/orders'
import SingleOrder from './containers/orders/single'

const GlobalStyles = createGlobalStyle`
    body {
        margin: 0;
        padding: 0;
        background: whitesmoke;
    }

    .center {
        width: 100%;
        height: 60vh;
        display: flex;
        align-items: center;
        justify-content: center;
    }

    .ant-layout-sider {
        z-index: 1;
    }

    .product-item {
        border: 1px solid #d8d8d8;
        border-radius: 3px;
        background: white;
        transition: 0.3s border;

        .open {
            font-size: 12px;
            margin-top: 3px;
            z-index: 1000;
        }

        &:hover {
            border: 1px solid #b3b3b3;
            cursor: move;
        }

        .image {
            width: 100%;
            height: 163px;
            object-fit: contain;
            background: white;

            @media only screen and (max-width: 600px) {
                height: 120px;
            }
        }

        .info {
            padding: 10px 12px;

            .name {
                font-size: 14px;
                color: gray;

                @media only screen and (max-width: 600px) {
                    font-size: 12px;
                }
            }

            .price {
                color: black;
                margin-top: 3px;
                font-size: 12px;
            }

            .archive {
                color: red;
                font-size: 12px;

                @media only screen and (max-width: 600px) {
                    font-size: 10px;
                }
            }

            .category {
                font-size: 12px;
                margin-top: 3px;
                color: gray;

                @media only screen and (max-width: 600px) {
                    font-size: 10px;
                }
            }
        }
    }
`

const App = () => {
    return (
        <ApolloProvider client={apollo}>
            <ConfigProvider locale={ru}>
                <Router>
                    <Switch>
                        <Route path="/" exact component={(props) => withLayout(props, Profile)} />
                        <Route
                            path="/profile/password"
                            exact
                            component={(props) => withLayout(props, Password)}
                        />
                        <Route
                            path="/profile/edit"
                            exact
                            component={(props) => withLayout(props, EditProfile)}
                        />
                        <Route
                            path="/categories"
                            exact
                            component={(props) => withLayout(props, Categories)}
                        />
                        <Route
                            path="/categories/add"
                            exact
                            component={(props) => withLayout(props, AddCategory)}
                        />
                        <Route
                            path="/categories/:id"
                            exact
                            component={(props) => withLayout(props, Subcategories)}
                        />
                        <Route
                            path="/products"
                            exact
                            component={(props) => withLayout(props, Products)}
                        />
                        <Route
                            path="/products/add"
                            exact
                            component={(props) => withLayout(props, AddProduct)}
                        />
                        <Route
                            path="/products/:id"
                            exact
                            component={(props) => withLayout(props, SingleProduct)}
                        />
                        <Route
                            path="/products/:id/edit"
                            exact
                            component={(props) => withLayout(props, EditProduct)}
                        />
                        <Route
                            path="/orders"
                            exact
                            component={(props) => withLayout(props, Orders)}
                        />
                        <Route
                            path="/orders/:id"
                            exact
                            component={(props) => withLayout(props, SingleOrder)}
                        />
                        <Route path="/login" exact component={Login} />
                    </Switch>
                </Router>
                <GlobalStyles />
            </ConfigProvider>
        </ApolloProvider>
    )
}

ReactDOM.render(<App />, document.getElementById('root'))

serviceWorker.unregister()
