import React from 'react'
import styled from 'styled-components'
import { Form as AntForm, Input, Upload, Button, message, Switch } from 'antd'
import { UploadOutlined } from '@ant-design/icons'
import { useQuery, useMutation } from '@apollo/react-hooks'

import Top from '../../components/Top'
import { GET_SHOP } from '../../gqls/shop/queries'
import { EDIT_SHOP } from '../../gqls/shop/mutations'

const Form = styled(AntForm)`
    max-width: 500px;

    .ant-form-item-extra {
        margin-top: 5px;
    }
`

const rules = {
    required: {
        required: true,
        message: 'Обязательное поле'
    }
}

const EditProfile = ({ history }) => {
    const [form] = Form.useForm()

    const [editShop, { loading: editing }] = useMutation(EDIT_SHOP, {
        onCompleted: () => {
            message.success('Информация сохранена')
            history.push('/')
        },
        onError: (err) => {
            console.error(err)
            if (Array.isArray(err.graphQLErrors)) {
                err.graphQLErrors.forEach((item) => {
                    if (item.message === 'already-exist') {
                        message.error('Магазин с такой ссылкой уже существует')
                    } else {
                        message.error('Не удалось изменить информацию')
                    }
                })
            } else {
                message.error('Не удалось изменить информацию')
            }
        }
    })
    const { data, loading } = useQuery(GET_SHOP, {
        onCompleted: ({ shop }) => {
            form.setFieldsValue({
                name: shop.name,
                description: shop.description,
                contactsForUsers: shop.contactsForUsers,
                contactsWhatsapp: shop.contactsWhatsapp,
                url: shop.url,
                delivery: shop.delivery
            })
            if (shop.logo) {
                form.setFieldsValue({
                    logo: [
                        {
                            uid: '-1',
                            name: shop.logo,
                            status: 'done',
                            url: `/uploads/${shop.logo}`,
                            uploaded: true
                        }
                    ]
                })
            }
        }
    })

    const handleSubmit = (values) => {
        const variables = {
            ...values,
            logo: values.logo
                ? values.logo[0]
                    ? values.logo[0].originFileObj
                        ? values.logo[0].originFileObj
                        : values.logo[0]
                    : null
                : null,
            id: data.shop.id
        }
        editShop({
            variables
        })
    }

    const normFile = (e) => {
        if (Array.isArray(e)) {
            return e.length > 0 ? [e[e.length - 1]] : []
        }
        return e && e.fileList && e.fileList.length > 0 && [e.fileList[e.fileList.length - 1]]
    }

    return (
        <>
            <Top title="Изменить информацию" />
            <Form form={form} name="edit-profile-form" layout="vertical" onFinish={handleSubmit}>
                <Form.Item label="Название" name="name" rules={[rules.required]}>
                    <Input disabled={loading} />
                </Form.Item>
                <Form.Item
                    label="Номер телефона"
                    extra="Пример: 79241112233. Без пробелов, без других знаков. Если есть несколько номеров, напишите через запятую"
                    name="contactsForUsers"
                >
                    <Input disabled={loading} />
                </Form.Item>
                <Form.Item
                    label="Номер Whatsapp"
                    extra="Пример: +79241112233. Без пробелов, без других знаков. Только ОДИН номер"
                    name="contactsWhatsapp"
                >
                    <Input disabled={loading} />
                </Form.Item>
                <Form.Item label="Есть доставка" valuePropName="checked" name="delivery">
                    <Switch disabled={loading} />
                </Form.Item>
                <Form.Item
                    label="Ссылка"
                    name="url"
                    rules={[
                        () => ({
                            validator(_, value) {
                                if (!value) {
                                    return Promise.resolve()
                                } else {
                                    if (value.includes('http') || value.includes('/')) {
                                        return Promise.reject(
                                            'Введите просто название ссылки без символов'
                                        )
                                    } else {
                                        return Promise.resolve()
                                    }
                                }
                            }
                        })
                    ]}
                    extra="Придумайте ссылку для вашей страницы. Ваша ссылка будет выглядеть так: https://beonline.me/ссылка"
                >
                    <Input disabled={loading} addonBefore="/" />
                </Form.Item>
                <Form.Item label="Описание" name="description">
                    <Input.TextArea disabled={loading} autoSize={{ minRows: 4 }} />
                </Form.Item>
                <Form.Item
                    name="logo"
                    label="Логотип"
                    valuePropName="fileList"
                    getValueFromEvent={normFile}
                    extra="Предпочтительный размер логотипа 500 на 500 пикселей. Если изображение будет другого разрешения, сервер обрежет его"
                >
                    <Upload disabled={loading} accept="image/*" beforeUpload={() => false}>
                        <Button icon={<UploadOutlined />}>Загрузить</Button>
                    </Upload>
                </Form.Item>
                <Button loading={editing} disabled={loading} type="primary" htmlType="submit">
                    Отправить
                </Button>
            </Form>
        </>
    )
}

export default EditProfile
