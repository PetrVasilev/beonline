import React from 'react'
import { Button } from 'antd'
import styled from 'styled-components'
import { Link } from 'react-router-dom'
import { useQuery } from '@apollo/react-hooks'
import moment from 'moment'

import Top from '../../components/Top'
import LabeledValue from '../../components/LabeledValue'
import { GET_SHOP } from '../../gqls/shop/queries'
import { getAccessView, DOMAIN } from '../../utils'

const View = styled.div`
    margin-bottom: 17px;

    .logo {
        object-fit: cover;
        height: 170px;
        width: 170px;
        border: 1px solid #cccccc;
    }

    .help-text {
        font-size: 13px;
        margin-top: -5px;
        color: gray;
    }
`

const Actions = styled.div`
    display: flex;
    flex-direction: row;

    .edit-button {
        margin-right: 10px;

        @media only screen and (max-width: 330px) {
            margin-bottom: 10px;
        }
    }

    @media only screen and (max-width: 330px) {
        flex-direction: column;
    }
`

const Main = () => {
    const {
        data: { shop }
    } = useQuery(GET_SHOP)

    const url = shop.url ? shop.url : shop.id

    return (
        <>
            <Top
                title="Личный кабинет"
                helpText={`Нажмите "Редактировать", чтобы изменить информацию вашего магазина`}
            />

            <View>
                <LabeledValue
                    label="Логотип"
                    value={
                        <img
                            src={shop.logo ? `/uploads/${shop.logo}` : '/default-image.jpg'}
                            alt="logo"
                            className="logo"
                        />
                    }
                />
                <LabeledValue label="Электронный адрес" value={shop.email} />
                <LabeledValue label="Название" value={shop.name} />
                <LabeledValue
                    label="Дата регистрации"
                    value={moment(shop.createdAt).format('DD.MM.YYYY HH:mm')}
                />
                <LabeledValue
                    label="Ссылка"
                    value={
                        <a href={`${DOMAIN}/${url}`} target="_blank" rel="noopener noreferrer">
                            {`${DOMAIN}/${url}`}
                        </a>
                    }
                />
                <LabeledValue label="Номер телефона" value={shop.contactsForUsers} />
                <LabeledValue label="Номер Whatsapp" value={shop.contactsWhatsapp} />
                <LabeledValue label="Есть доставка" value={shop.delivery ? 'Есть' : 'Нет'} />
                <LabeledValue label="Описание" value={shop.description} />
                <LabeledValue label="Доступ" value={getAccessView(shop.access)} />
                {!shop.access && (
                    <div className="help-text">
                        Адимнистратор закрыл вам доступ. Ваш сайт сейчас не доступен
                    </div>
                )}
            </View>

            <Actions>
                <Link to="/profile/edit">
                    <Button className="edit-button" type="primary" ghost>
                        Редактировать
                    </Button>
                </Link>
                <Link to="/profile/password">
                    <Button type="primary" ghost>
                        Изменить пароль
                    </Button>
                </Link>
            </Actions>
        </>
    )
}

export default Main
