import React from 'react'
import styled from 'styled-components'
import { Form, Card as AntCard, Input, Button as AntButton, message } from 'antd'
import { useMutation } from '@apollo/react-hooks'

import { LOGIN_SHOP } from '../gqls/shop/mutations'

const requiredRule = {
    required: true,
    message: 'Обязательное поле'
}

const Login = ({ history }) => {
    const [login, { loading }] = useMutation(LOGIN_SHOP, {
        onCompleted: ({ loginShop: { token } }) => {
            localStorage.setItem('token', token)
            const url = new URL(window.location.href)
            const after = url.searchParams.get('after')
            if (after) {
                if (after === 'password') {
                    history.replace(`/profile/${after}`)
                } else {
                    history.replace(`/${after}`)
                }
            } else {
                history.replace('/')
            }
        },
        onError: (err) => {
            console.error(err)
            message.error('Неправильный электронный адрес или пароль')
        }
    })

    const handleSubmitForm = (values) => login({ variables: values })

    return (
        <Wrapper>
            {/* <Logo src={`/logo.png`} /> */}
            <Card>
                <Form
                    onFinish={handleSubmitForm}
                    layout="vertical"
                    name="login"
                    initialValues={{
                        remember: true
                    }}
                >
                    <Form.Item
                        colon={false}
                        label="Email"
                        name="email"
                        rules={[
                            requiredRule,
                            {
                                type: 'email',
                                message: 'Введите правильный Email'
                            }
                        ]}
                    >
                        <Input placeholder="Введите электронный адрес..." />
                    </Form.Item>
                    <Form.Item label="Пароль" name="password" rules={[requiredRule]}>
                        <Input.Password placeholder="Введите пароль..." />
                    </Form.Item>
                    <Button loading={loading} type="primary" htmlType="submit">
                        Войти
                    </Button>
                </Form>
            </Card>
        </Wrapper>
    )
}

const Card = styled(AntCard)`
    width: 400px;

    @media only screen and (max-width: 420px) {
        width: 95%;
    }
`

const Button = styled(AntButton)`
    margin-bottom: 10px;
    margin-top: 5px;
`

const Wrapper = styled.div`
    display: flex;
    align-items: center;
    justify-content: center;
    flex-direction: column;
    height: 100vh;

    @media only screen and (max-width: 420px) {
        justify-content: flex-start;
        padding-top: 30px;
    }
`

// const Logo = styled.img`
//     height: 40px;
//     margin-bottom: 22px;
// `

export default Login
