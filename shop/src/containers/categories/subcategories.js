import React, { useState } from 'react'
import styled from 'styled-components'
import { useQuery, useMutation } from '@apollo/react-hooks'
import { Spin, Empty, Form as AntForm, Input, Button, message, Popconfirm } from 'antd'
import { DragDropContext, Droppable, Draggable } from 'react-beautiful-dnd'

import Top from '../../components/Top'
import LabeledValue from '../../components/LabeledValue'
import { GET_CATEGORY } from '../../gqls/category/queries'
import {
    CREATE_SUBCATEGORY,
    DELETE_SUBCATEGORY,
    SET_SORT_SUBCATEGORY
} from '../../gqls/category/mutations'

const Form = styled(AntForm)`
    max-width: 400px;
    display: flex;
    flex-direction: row;
    margin-top: 15px;

    .form-item {
        margin-bottom: 0;
        width: 100%;
        margin-right: 10px;
    }
`

const View = styled.div`
    .subcategory {
        display: flex;
        flex-direction: row;
        align-items: center;
        justify-content: space-between;
        max-width: 400px;
        margin-bottom: 10px;

        &:last-child {
            margin-bottom: 0;
        }

        .title {
            cursor: move;
            transition: 0.3s opacity;

            &:hover {
                opacity: 0.7;
            }
        }
    }
`

const Subcategories = ({ match }) => {
    const [subcategories, setSubcategories] = useState([])

    const { data, loading } = useQuery(GET_CATEGORY, {
        variables: {
            categoryId: parseInt(match.params.id)
        },
        onCompleted: (data) => {
            const category = data ? data.category : null
            if (category) {
                if (category.subcategories) {
                    setSubcategories(
                        category.subcategories.sort((a, b) =>
                            a.sortNumber > b.sortNumber ? 1 : a.sortNumber < b.sortNumber ? -1 : 0
                        )
                    )
                }
            }
        }
    })

    const category = data ? data.category : null

    const [form] = Form.useForm()

    const [createSubcategory, { loading: creating }] = useMutation(CREATE_SUBCATEGORY, {
        onCompleted: ({ createSubcategory: subcategory }) => {
            setSubcategories([...subcategories, subcategory])
            form.setFieldsValue({
                name: ''
            })
        },
        onError: (err) => {
            console.error(err)
            message.error('Не удалось добавить подкатегорию')
        }
    })

    const [setSortSubcategory] = useMutation(SET_SORT_SUBCATEGORY)

    const handleSubmit = (values) => {
        createSubcategory({
            variables: {
                categoryId: parseInt(match.params.id),
                name: values.name
            }
        })
    }

    const onDragEnd = ({ destination, source }) => {
        if (!destination) return null
        const fromIndex = source.index
        const toIndex = destination.index
        const arr = reorder(subcategories, fromIndex, toIndex)
        const orderedArr = arr.map((item, index) => {
            return {
                subcategoryId: item.id,
                sortNumber: index + 1
            }
        })
        setSubcategories(arr)
        setSortSubcategory({
            variables: {
                arr: orderedArr
            }
        })
    }

    return (
        <>
            <Top title="Подкатегории" />
            {loading ? (
                <div className="center">
                    <Spin />
                </div>
            ) : !category ? (
                <div className="center">
                    <Empty />
                </div>
            ) : (
                <>
                    <LabeledValue label="Категория" value={category.name} />
                    {subcategories.length === 0 ? (
                        <div className="no-data">Нет подкатегорий</div>
                    ) : (
                        <DragDropContext onDragEnd={onDragEnd}>
                            <Droppable droppableId="droppable">
                                {(provided) => (
                                    <View {...provided.droppableProps} ref={provided.innerRef}>
                                        {subcategories.map((item, index) => (
                                            <Draggable
                                                key={item.id}
                                                draggableId={item.id.toString()}
                                                index={index}
                                            >
                                                {(provided) => (
                                                    <>
                                                        <div
                                                            ref={provided.innerRef}
                                                            {...provided.draggableProps}
                                                            {...provided.dragHandleProps}
                                                            key={item.id}
                                                            className="subcategory"
                                                        >
                                                            <div className="title">
                                                                {index + 1}. {item.name}
                                                            </div>
                                                            <div>
                                                                <DeleteButton
                                                                    subcategories={subcategories}
                                                                    setSubcategories={
                                                                        setSubcategories
                                                                    }
                                                                    id={item.id}
                                                                />
                                                            </div>
                                                        </div>
                                                        {provided.placeholder}
                                                    </>
                                                )}
                                            </Draggable>
                                        ))}
                                        {provided.placeholder}
                                    </View>
                                )}
                            </Droppable>
                        </DragDropContext>
                    )}

                    <Form form={form} onFinish={handleSubmit}>
                        <Form.Item
                            rules={[{ required: true, message: 'Обязательное поле' }]}
                            className="form-item"
                            name="name"
                        >
                            <Input placeholder="Название подкатегории" />
                        </Form.Item>
                        <Button loading={creating} type="primary" htmlType="submit">
                            Добавить
                        </Button>
                    </Form>
                </>
            )}
        </>
    )
}

const DeleteButton = ({ subcategories, id, setSubcategories }) => {
    const [deleteSubcategory] = useMutation(DELETE_SUBCATEGORY, {
        onCompleted: () => {
            setSubcategories(subcategories.filter((item) => item.id !== id))
            message.success('Подкатегория удалена')
        },
        onError: (err) => {
            message.error('Не удалось удалить подкатегорию')
            console.error(err)
        }
    })

    return (
        <Popconfirm
            title="Удалить?"
            onConfirm={() =>
                deleteSubcategory({
                    variables: {
                        id
                    }
                })
            }
        >
            <Button style={{ fontSize: 12 }} type="danger" ghost size="small">
                Удалить
            </Button>
        </Popconfirm>
    )
}

const reorder = (list, startIndex, endIndex) => {
    const result = Array.from(list)
    const [removed] = result.splice(startIndex, 1)
    result.splice(endIndex, 0, removed)
    return result
}

export default Subcategories
