import React from 'react'
import styled from 'styled-components'
import { Form as AntForm, Input, Button, message } from 'antd'
import { useMutation } from '@apollo/react-hooks'

import Top from '../../components/Top'
import { CREATE_CATEGORY } from '../../gqls/category/mutations'

const Form = styled(AntForm)`
    max-width: 400px;
`

const AddCategory = ({ history }) => {
    const [createCategory, { loading }] = useMutation(CREATE_CATEGORY, {
        onCompleted: () => {
            message.success('Категория создана')
            history.push('/categories')
        },
        onError: (err) => {
            console.error(err)
            message.error('Не удалось добавить категорию')
        }
    })

    const handleSubmit = (values) => {
        createCategory({
            variables: values
        })
    }

    return (
        <>
            <Top title="Добавить категорию" />
            <Form onFinish={handleSubmit} name="create-name-form" layout="vertical">
                <Form.Item
                    label="Название"
                    rules={[{ required: true, message: 'Обязательное поле' }]}
                    name="name"
                >
                    <Input />
                </Form.Item>
                <Button loading={loading} type="primary" htmlType="submit">
                    Отправить
                </Button>
            </Form>
        </>
    )
}

export default AddCategory
