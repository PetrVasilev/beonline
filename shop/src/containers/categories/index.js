import React, { useState } from 'react'
import styled from 'styled-components'
import { Button, Popconfirm, message, Spin, Empty } from 'antd'
import { useQuery, useMutation } from '@apollo/react-hooks'
import { Link } from 'react-router-dom'
import { DeleteOutlined } from '@ant-design/icons'
import { DragDropContext, Droppable, Draggable } from 'react-beautiful-dnd'

import Top from '../../components/Top'
import { GET_CATEGORIES } from '../../gqls/category/queries'
import { DELETE_CATEGORY, SET_SORT_CATEGORY } from '../../gqls/category/mutations'
import { GET_SHOP } from '../../gqls/shop/queries'

const CategoryWrapper = styled.div`
    display: flex;
    flex-direction: row;
    align-items: center;
    justify-content: space-between;
    padding: 10px 15px;
    border: 1px solid silver;
    margin-bottom: 10px;
    cursor: move;
    background: white;

    &:hover {
        border: 1px solid gray;
    }

    @media only screen and (max-width: 500px) {
        flex-direction: column;
        align-items: flex-start;
    }

    .right {
        display: flex;
        flex-direction: row;
        align-items: center;
        justify-content: flex-end;

        .subcategories {
            margin-right: 10px;
        }
    }
`

const Category = ({ data, refetch, provided }) => {
    const [deleteCategory, { loading }] = useMutation(DELETE_CATEGORY, {
        onCompleted: () => {
            message.success(`Категория ${data.name} удалена`)
            refetch()
        },
        onError: () => {
            message.error('Не удалось удалить категорию')
        }
    })
    const handleDelete = () => {
        deleteCategory({
            variables: {
                id: data.id
            }
        })
    }
    return (
        <CategoryWrapper
            ref={provided.innerRef}
            {...provided.draggableProps}
            {...provided.dragHandleProps}
        >
            <div className="name">{data.name}</div>
            <div className="right">
                <Link className="subcategories" to={`/categories/${data.id}`}>
                    Подкатегории
                </Link>
                <Popconfirm title="Удалить?" onConfirm={handleDelete}>
                    <Button
                        loading={loading}
                        icon={<DeleteOutlined />}
                        type="danger"
                        size="small"
                        ghost
                    />
                </Popconfirm>
            </div>
        </CategoryWrapper>
    )
}

const Categories = () => {
    const [categories, setCategories] = useState([])

    const {
        data: { shop }
    } = useQuery(GET_SHOP)

    const { refetch, loading } = useQuery(GET_CATEGORIES, {
        variables: { shopId: shop.id },
        fetchPolicy: 'network-only',
        onCompleted: (data) => {
            setCategories(data.categories)
        },
        onError: (err) => {
            console.error(err)
            message.error('Не удалось загрузить категории')
        }
    })

    const [setSortCategory] = useMutation(SET_SORT_CATEGORY)

    const onDragEnd = ({ destination, source }) => {
        if (!destination) return null
        const fromIndex = source.index
        const toIndex = destination.index
        const arr = reorder(categories, fromIndex, toIndex)
        const orderedArr = arr.map((item, index) => {
            return {
                categoryId: item.id,
                sortNumber: index + 1
            }
        })
        setCategories(arr)
        setSortCategory({
            variables: {
                arr: orderedArr
            }
        })
    }

    return (
        <>
            <Top title="Категории" action={<Link to="/categories/add">+ Добавить</Link>} />
            {loading ? (
                <div className="center">
                    <Spin />
                </div>
            ) : categories.length === 0 ? (
                <div className="center">
                    <Empty />
                </div>
            ) : (
                <DragDropContext onDragEnd={onDragEnd}>
                    <Droppable droppableId="droppable">
                        {(provided) => (
                            <div {...provided.droppableProps} ref={provided.innerRef}>
                                {categories.map((item, index) => (
                                    <Draggable
                                        key={item.id}
                                        draggableId={item.id.toString()}
                                        index={index}
                                    >
                                        {(provided) => (
                                            <>
                                                <Category
                                                    provided={provided}
                                                    refetch={refetch}
                                                    data={item}
                                                    key={item.id}
                                                />
                                                {provided.placeholder}
                                            </>
                                        )}
                                    </Draggable>
                                ))}
                                {provided.placeholder}
                            </div>
                        )}
                    </Droppable>
                </DragDropContext>
            )}
        </>
    )
}

const reorder = (list, startIndex, endIndex) => {
    const result = Array.from(list)
    const [removed] = result.splice(startIndex, 1)
    result.splice(endIndex, 0, removed)
    return result
}

export default Categories
