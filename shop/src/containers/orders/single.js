import React, { useState } from 'react'
import styled from 'styled-components'
import { useQuery, useMutation } from '@apollo/react-hooks'
import { Spin, Empty, Button, Popconfirm, Modal, Form, Select, message } from 'antd'
import moment from 'moment'

import { GET_ORDER, SET_STATUS_ORDER, DELETE_ORDER } from '../../gqls/order'
import { getOrderStatusView, convertPrice } from '../../utils'
import Top from '../../components/Top'
import LabeledValue from '../../components/LabeledValue'

const View = styled.div`
    .products-table {
        width: 100%;
        flex-direction: column;

        .value {
            width: auto;
            margin-top: 8px;
            margin-bottom: 5px;

            @media only screen and (max-width: 800px) {
                overflow-x: auto;
                width: 100%;
            }
        }
    }
`

const Products = styled.table`
    margin-top: 10px;
    border-collapse: collapse;
    width: 100%;

    th,
    td {
        border: 1px solid #ddd;
        padding: 10px;
        min-width: 100px;
    }

    tr:nth-child(even) {
        background-color: #f2f2f2;
    }

    th {
        padding-top: 12px;
        padding-bottom: 12px;
        font-weight: 500;
        text-align: left;
        background-color: #f2f2f2;
        color: black;
    }

    .name {
        min-width: 200px;
    }
`

const Actions = styled.div`
    display: flex;
    flex-direction: row;
    margin-top: 20px;

    .delete-button {
        margin-left: 10px;
    }
`

const SingleOrder = ({ match, history }) => {
    const id = match.params.id

    const [deleteOrder, { loading: deleting }] = useMutation(DELETE_ORDER, {
        onError: (err) => {
            console.error(err)
            message.error('Не удалось удалить')
        },
        onCompleted: () => {
            message.success('Заказ удален')
            history.replace('/orders')
        }
    })
    const [visibleModal, setVisibleModal] = useState(false)

    const { data, loading } = useQuery(GET_ORDER, {
        variables: { id: parseInt(id) }
    })

    const order = data ? data.order : null

    let total = 0

    const handleDelete = () => {
        deleteOrder({
            variables: {
                id: parseInt(id)
            }
        })
    }

    const cartItems = order
        ? order.cartItems.map((item) => {
              const price = item.count * item.product.price
              total += price
              return (
                  <tr key={item.id}>
                      <td>
                          <a
                              target="_blank"
                              rel="noopener noreferrer"
                              href={`/products/${item.product.id}`}
                              alt={item.product.name}
                          >
                              {item.product.name}
                          </a>
                      </td>
                      <td>{item.count} шт.</td>
                      <td>{convertPrice(price)} руб.</td>
                  </tr>
              )
          })
        : []

    return (
        <>
            <Top title={`Заказ № ${id}`} />
            {loading ? (
                <div className="center">
                    <Spin />
                </div>
            ) : !order ? (
                <div className="center">
                    <Empty />
                </div>
            ) : (
                <View>
                    <LabeledValue label="Имя" value={order.userName} />
                    <LabeledValue label="Номер телефона" value={order.userPhone} />
                    <LabeledValue label="Адрес" value={order.userAddress} />
                    <LabeledValue label="Email" value={order.userEmail} />
                    <LabeledValue
                        label="Дата создания"
                        value={moment(order.createdAt).format('DD.MM.YYYY HH:mm')}
                    />
                    <LabeledValue label="Статус" value={getOrderStatusView(order.status)} />
                    <LabeledValue
                        label="Товары"
                        className="products-table"
                        valueClassName="value"
                        value={
                            <Products>
                                <thead>
                                    <tr>
                                        <th className="name">Название</th>
                                        <th>Количество</th>
                                        <th>Цена</th>
                                    </tr>
                                </thead>
                                <tbody>{cartItems}</tbody>
                            </Products>
                        }
                    />
                    <LabeledValue label="Всего (Цена)" value={convertPrice(total) + ' руб.'} />

                    <Actions>
                        <Button onClick={() => setVisibleModal(true)} type="primary" ghost>
                            Изменить статус
                        </Button>
                        <Popconfirm title="Удалить?" onConfirm={handleDelete}>
                            <Button
                                loading={deleting}
                                className="delete-button"
                                type="danger"
                                ghost
                            >
                                Удалить
                            </Button>
                        </Popconfirm>
                    </Actions>
                </View>
            )}
            <StatusModal
                order={order}
                visible={visibleModal}
                onClose={() => setVisibleModal(false)}
            />
        </>
    )
}

const StatusModal = ({ visible, onClose, order }) => {
    const [setStatusOrder, { loading }] = useMutation(SET_STATUS_ORDER, {
        onError: (err) => {
            console.error(err)
            message.error('Не удалось изменить статус')
        },
        onCompleted: () => {
            onClose()
            message.success('Статус изменен')
        }
    })

    const handleSubmit = (values) => {
        setStatusOrder({
            variables: {
                id: order.id,
                status: values.status
            }
        })
    }

    return (
        <Modal
            title="Изменить статус"
            visible={visible}
            onCancel={onClose}
            footer={[
                <Button onClick={onClose} key="close">
                    Закрыть
                </Button>,
                <Button
                    loading={loading}
                    type="primary"
                    key="submit"
                    htmlType="submit"
                    form="set-status-form"
                >
                    Сохранить
                </Button>
            ]}
        >
            <Form onFinish={handleSubmit} name="set-status-form" layout="vertical">
                <Form.Item
                    name="status"
                    label="Статус"
                    rules={[{ required: true, message: 'Обязательно' }]}
                >
                    <Select placeholder="Выбрать вариант">
                        <Select.Option value={0}>Новый</Select.Option>
                        <Select.Option value={1}>В процессе</Select.Option>
                        <Select.Option value={2}>Выполнен</Select.Option>
                        <Select.Option value={3}>Отменен</Select.Option>
                    </Select>
                </Form.Item>
            </Form>
        </Modal>
    )
}

export default SingleOrder
