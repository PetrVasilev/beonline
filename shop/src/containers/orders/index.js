import React, { useState, useEffect } from 'react'
import styled from 'styled-components'
import { useQuery } from '@apollo/react-hooks'
import { Table as AntTable, Input, Select } from 'antd'
import moment from 'moment'
import { Link } from 'react-router-dom'

import Top from '../../components/Top'
import { GET_ORDERS } from '../../gqls/order'
import { getOrderStatusView } from '../../utils'

const Table = styled(AntTable)``

const Filters = styled.div`
    margin-bottom: 20px;
    display: flex;
    flex-direction: row;
    align-items: center;

    @media only screen and (max-width: 700px) {
        flex-direction: column;
        align-items: flex-start;
    }

    .search {
        max-width: 250px;
    }

    .search-id {
        max-width: 250px;
        margin-left: 15px;

        @media only screen and (max-width: 700px) {
            margin-left: 0;
            margin-top: 10px;
        }
    }

    .filter-status {
        max-width: 250px;
        margin-left: 15px;

        @media only screen and (max-width: 700px) {
            margin-left: 0;
            margin-top: 10px;
        }
    }
`

const limit = 100

const Orders = () => {
    const [currentPage, setCurrentPage] = useState(1)
    const [search, setSearch] = useState()
    const [filterId, setFilterId] = useState()
    const [select, setSelect] = useState()

    const { data, loading, refetch } = useQuery(GET_ORDERS, {
        variables: {
            first: limit,
            skip: 0
        }
    })

    const orders = data ? data.orders : []
    const count = data ? data.ordersCount : 0

    useEffect(() => {
        getOrders({ skip: 0 })
        // eslint-disable-next-line
    }, [select])

    const handleChangeTable = (pagination) => {
        setCurrentPage(pagination.current)
        getOrders({
            skip: (pagination.current - 1) * limit
        })
    }

    const getOrders = ({ skip = 0 }) => {
        refetch({
            search,
            id: filterId ? parseInt(filterId) : undefined,
            status: select,
            skip,
            first: limit
        })
    }

    return (
        <>
            <Top title="Заказы" />
            <Filters>
                <Input.Search
                    placeholder="Поиск..."
                    value={search}
                    onChange={(e) => setSearch(e.target.value)}
                    onSearch={() => getOrders({ skip: 0 })}
                    className="search"
                />
                <Input.Search
                    placeholder="Поиск по ID..."
                    type="number"
                    value={filterId}
                    onChange={(e) => setFilterId(e.target.value)}
                    onSearch={() => getOrders({ skip: 0 })}
                    className="search-id"
                />
                <Select
                    placeholder="Фильтрация по статусу"
                    onChange={(value) => setSelect(value)}
                    value={select}
                    allowClear
                    className="filter-status"
                >
                    <Select.Option value={0}>Новый</Select.Option>
                    <Select.Option value={1}>В процессе</Select.Option>
                    <Select.Option value={2}>Выполнен</Select.Option>
                    <Select.Option value={3}>Отменен</Select.Option>
                </Select>
            </Filters>
            <Table
                loading={loading}
                columns={columns}
                dataSource={orders}
                rowKey={(obj) => obj.id}
                size={window.innerWidth < 500 ? 'small' : 'default'}
                scroll={{ x: 1000 }}
                onChange={handleChangeTable}
                pagination={{
                    total: count,
                    current: currentPage,
                    pageSize: limit
                }}
            />
        </>
    )
}

const columns = [
    {
        title: '№',
        dataIndex: 'id',
        key: 'id',
        render: (id) => <Link to={`/orders/${id}`}>{id}</Link>
    },
    {
        title: 'Статус',
        dataIndex: 'status',
        key: 'status',
        render: (status) => getOrderStatusView(status)
    },
    {
        title: 'Имя',
        dataIndex: 'userName',
        key: 'userName'
    },
    {
        title: 'Номер',
        dataIndex: 'userPhone',
        key: 'userPhone'
    },
    {
        title: 'Адрес',
        dataIndex: 'userAddress',
        key: 'userAddress'
    },
    {
        title: 'Email',
        dataIndex: 'userEmail',
        key: 'userEmail'
    },
    {
        title: 'Создан',
        dataIndex: 'createdAt',
        key: 'createdAt',
        render: (created) => moment(created).format('DD.MM.YYYY HH:mm')
    }
]

export default Orders
