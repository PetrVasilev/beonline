import React, { useState } from 'react'
import styled from 'styled-components'
import { Form as AntForm, Input, InputNumber, Select, Button, Upload, message } from 'antd'
import { useQuery, useMutation } from '@apollo/react-hooks'
import { UploadOutlined } from '@ant-design/icons'

import Top from '../../components/Top'
import { GET_CATEGORIES } from '../../gqls/category/queries'
import { GET_SHOP } from '../../gqls/shop/queries'
import { CREATE_PRODUCT } from '../../gqls/product/mutations'

const Form = styled(AntForm)`
    max-width: 500px;

    .ant-form-item-extra {
        margin-top: 5px;
    }
`

const rules = {
    required: {
        required: true,
        message: 'Обязательное поле'
    }
}

const AddProduct = ({ history }) => {
    const [form] = Form.useForm()

    const [subcategories, setSubcategories] = useState([])

    const {
        data: { shop }
    } = useQuery(GET_SHOP)
    const { data: categoriesData, loading: gettingCategories } = useQuery(GET_CATEGORIES, {
        variables: { shopId: shop.id }
    })
    const [createProduct, { loading: creating }] = useMutation(CREATE_PRODUCT, {
        onError: (err) => {
            console.error(err)
            message.error('Не удалось добавить товар')
        },
        onCompleted: () => {
            message.success('Товар добавлен')
            history.push('/products')
        }
    })

    const categories = categoriesData ? categoriesData.categories : []

    const normFile = (e) => {
        if (Array.isArray(e)) {
            return e
        }
        return e && e.fileList
    }

    const handleSubmit = (values) => {
        const variables = {
            ...values,
            images: values.images ? values.images.map((item) => item.originFileObj) : null
        }
        createProduct({ variables })
    }

    return (
        <>
            <Top title="Добавить товар" />
            <Form onFinish={handleSubmit} form={form} name="add-product-form" layout="vertical">
                <Form.Item label="Название" name="name" rules={[rules.required]}>
                    <Input />
                </Form.Item>
                <Form.Item label="Цена ₽" name="price" rules={[rules.required]}>
                    <InputNumber style={{ width: '100%' }} decimalSeparator="." />
                </Form.Item>
                <Form.Item label="Описание" name="description">
                    <Input.TextArea autoSize={{ minRows: 4 }} />
                </Form.Item>
                <Form.Item label="Категория" name="categoryId">
                    <Select
                        onChange={(id) => {
                            const filtered = categories.filter((item) => item.id === id)
                            if (filtered[0]) {
                                setSubcategories(filtered[0].subcategories)
                            } else {
                                setSubcategories([])
                            }
                        }}
                        disabled={gettingCategories}
                    >
                        {categories.map((item) => (
                            <Select.Option key={item.id} value={item.id}>
                                {item.name}
                            </Select.Option>
                        ))}
                    </Select>
                </Form.Item>
                <Form.Item label="Подкатегория" name="subcategoryId">
                    <Select disabled={gettingCategories}>
                        {subcategories.map((item) => (
                            <Select.Option key={item.id} value={item.id}>
                                {item.name}
                            </Select.Option>
                        ))}
                    </Select>
                </Form.Item>
                <Form.Item
                    name="images"
                    label="Изображения"
                    valuePropName="fileList"
                    getValueFromEvent={normFile}
                    extra={
                        <span>
                            Чтобы изображения быстро загружались и ваша страница не тормозила, нужно
                            загрузить изображения не более 200 - 300 кб. Можно воспользоваться
                            онлайн сервисом для компрессии изображений{' '}
                            <a
                                href="https://imagecompressor.com"
                                target="_blank"
                                rel="noopener noreferrer"
                            >
                                https://imagecompressor.com
                            </a>
                        </span>
                    }
                >
                    <Upload accept="image/*" multiple beforeUpload={() => false}>
                        <Button icon={<UploadOutlined />}>Загрузить</Button>
                    </Upload>
                </Form.Item>
                <Button loading={creating} htmlType="submit" type="primary">
                    Отправить
                </Button>
            </Form>
        </>
    )
}

export default AddProduct
