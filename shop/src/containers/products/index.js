import React, { useState, useEffect } from 'react'
import styled from 'styled-components'
import { useQuery, useMutation } from '@apollo/react-hooks'
import { Spin, Empty, Select } from 'antd'
import { Link } from 'react-router-dom'
import { SortableContainer, SortableElement } from 'react-sortable-hoc'
import arrayMove from 'array-move'

import Top from '../../components/Top'
import { GET_PRODUCTS } from '../../gqls/product/queries'
import { SET_SORT_PRODUCT } from '../../gqls/product/mutations'
import { GET_SHOP } from '../../gqls/shop/queries'
import { GET_CATEGORIES } from '../../gqls/category/queries'
import { convertPrice } from '../../utils'

const List = styled.div`
    display: grid;
    grid-template-columns: repeat(5, 1fr);
    grid-gap: 15px;

    @media only screen and (max-width: 1000px) {
        grid-template-columns: repeat(4, 1fr);
    }

    @media only screen and (max-width: 800px) {
        grid-template-columns: repeat(3, 1fr);
    }

    @media only screen and (max-width: 600px) {
        grid-template-columns: repeat(2, 1fr);
        grid-gap: 10px;
    }
`

const Filter = styled.div`
    margin-bottom: 20px;

    .selector {
        min-width: 200px;
        margin-right: 10px;

        @media only screen and (max-width: 600px) {
            width: 100%;
            margin-bottom: 10px;
            margin-right: 0;

            &:last-child {
                margin-bottom: 0;
            }
        }
    }
`

const Products = () => {
    const [products, setProducts] = useState([])
    const [categoryId, setCategoryId] = useState(undefined)
    const [subcategoryId, setSubcategoryId] = useState(undefined)
    const [archive, setArchive] = useState(false)
    const [subcategories, setSubcategories] = useState([])

    const {
        data: { shop }
    } = useQuery(GET_SHOP)
    const { refetch, loading } = useQuery(GET_PRODUCTS, {
        variables: {
            shopId: shop.id,
            archive
        },
        fetchPolicy: 'network-only',
        onCompleted: (data) => {
            const products = data ? data.products : []
            setProducts(products)
        }
    })
    const { data: categoriesData } = useQuery(GET_CATEGORIES, {
        variables: { shopId: shop.id }
    })

    const [setSortProduct] = useMutation(SET_SORT_PRODUCT)

    const categories = categoriesData ? categoriesData.categories : []

    useEffect(() => {
        refetch({
            categoryId,
            subcategoryId,
            shopId: shop.id,
            archive
        })
        // eslint-disable-next-line
    }, [categoryId, subcategoryId, archive])

    const onSortEnd = ({ oldIndex, newIndex }) => {
        const reordered = arrayMove(products, oldIndex, newIndex)
        setProducts(reordered)
        setSortProduct({
            variables: {
                arr: reordered.map((item, index) => ({
                    productId: item.id,
                    sortNumber: index
                }))
            }
        })
    }

    return (
        <>
            <Top title="Товары" action={<Link to="/products/add">+ Добавить</Link>} />
            <Filter>
                <Select
                    placeholder="Категория"
                    allowClear
                    value={categoryId}
                    className="selector"
                    onChange={(value) => {
                        const filtered = categories.filter((item) => item.id === value)
                        if (filtered[0]) {
                            setSubcategories(filtered[0].subcategories)
                        } else {
                            setSubcategories([])
                        }
                        setCategoryId(value)
                    }}
                >
                    {categories.map((item) => (
                        <Select.Option value={item.id} key={item.id}>
                            {item.name}
                        </Select.Option>
                    ))}
                </Select>
                <Select
                    placeholder="Подкатегория"
                    allowClear
                    value={subcategoryId}
                    className="selector"
                    onChange={(value) => setSubcategoryId(value)}
                >
                    {subcategories.map((item) => (
                        <Select.Option value={item.id} key={item.id}>
                            {item.name}
                        </Select.Option>
                    ))}
                </Select>
                <Select
                    placeholder="Статус архива"
                    value={archive ? 'yes' : 'no'}
                    className="selector"
                    onChange={(value) => setArchive(value === 'yes' ? true : false)}
                >
                    <Select.Option value={'yes'}>Архив</Select.Option>
                    <Select.Option value={'no'}>Доступные товары</Select.Option>
                </Select>
            </Filter>
            {loading ? (
                <div className="center">
                    <Spin />
                </div>
            ) : products.length === 0 ? (
                <div className="center">
                    <Empty />
                </div>
            ) : (
                <SortableList pressDelay={200} axis={'xy'} items={products} onSortEnd={onSortEnd} />
            )}
        </>
    )
}

const SortableList = SortableContainer(({ items: products }) => {
    return (
        <List>
            {products.map((item, index) => {
                return <SortableItem key={item.id} index={index} value={item} />
            })}
        </List>
    )
})

const SortableItem = SortableElement(({ value: item }) => {
    const firstImage = item.images && item.images[0]
    return (
        <div className="product-item">
            <img
                className="image"
                src={firstImage ? `/uploads/${firstImage}` : '/default-image.jpg'}
                style={{ opacity: firstImage ? 1 : 0.5 }}
                alt={item.name}
            />
            <div className="info">
                <div className="name">{item.name}</div>
                <div className="price">{convertPrice(item.price)} ₽</div>
                {item.archive && <div className="archive">В архиве</div>}
                {item.category && <div className="category">{item.category.name}</div>}
                {item.subcategory && <div className="category">{item.subcategory.name}</div>}
                <Link className="open" to={`/products/${item.id}`}>
                    Открыть
                </Link>
            </div>
        </div>
    )
})

export default Products
