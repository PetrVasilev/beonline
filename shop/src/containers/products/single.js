import React from 'react'
import styled from 'styled-components'
import { useQuery, useMutation } from '@apollo/react-hooks'
import { Spin, Tag, Carousel, Button, Popconfirm, message } from 'antd'
import moment from 'moment'
import { LeftCircleOutlined, RightCircleOutlined } from '@ant-design/icons'
import { Link } from 'react-router-dom'

import Top from '../../components/Top'
import LabeledValue from '../../components/LabeledValue'
import { GET_PRODUCT } from '../../gqls/product/queries'
import { DELETE_PRODUCT } from '../../gqls/product/mutations'
import { convertPrice } from '../../utils'

const View = styled.div`
    .ant-carousel {
        max-width: 500px;
        height: 40vh;
        margin-bottom: 40px;

        .slick-dots li button {
            background: rgba(0, 0, 0, 0.5);
        }

        .slick-dots-bottom {
            bottom: -20px;
            z-index: 1;
            margin-bottom: 0;
        }

        .slick-dots li.slick-active button {
            background: rgba(0, 0, 0, 0.6);
        }

        .slick-slide {
            overflow: hidden;
            height: 40vh;
        }

        .image {
            object-fit: contain;
            width: 100%;
            height: 40vh;
        }

        .arrow {
            position: absolute;
            z-index: 1;
            top: 50%;
            transform: translateY(-50%);
            cursor: pointer;

            .icon {
                color: rgba(0, 0, 0, 0.6);
                font-size: 20px;
            }
        }

        .next {
            right: 6px;
        }

        .prev {
            left: 6px;
        }
    }

    .single-image {
        height: 40vh;
        margin-bottom: 20px;
    }
`

const Actions = styled.div`
    margin-top: 17px;
`

const SampleNextArrow = ({ onClick }) => {
    return (
        <div className="arrow next" onClick={onClick}>
            <RightCircleOutlined className="icon" />
        </div>
    )
}

const SamplePrevArrow = ({ onClick }) => {
    return (
        <div className="arrow prev" onClick={onClick}>
            <LeftCircleOutlined className="icon" />
        </div>
    )
}

const SingleProduct = ({ match, history }) => {
    const { id } = match.params

    const { data, loading } = useQuery(GET_PRODUCT, {
        variables: { id: parseInt(id) },
        fetchPolicy: 'network-only'
    })
    const product = data ? data.product : null

    const [deleteProduct, { loading: deleting }] = useMutation(DELETE_PRODUCT, {
        onError: (err) => {
            console.error(err)
            message.error('Не удалось удалить товар')
        },
        onCompleted: () => {
            message.success(`Товар ${product.name} удален`)
            history.replace('/products')
        }
    })

    const handleDelete = () => {
        deleteProduct({
            variables: {
                id: parseInt(id)
            }
        })
    }

    return (
        <View>
            <Top title="Товар" />
            {loading ? (
                <div className="center">
                    <Spin />
                </div>
            ) : (
                <>
                    {product.images && product.images.length > 1 ? (
                        <Carousel
                            nextArrow={<SampleNextArrow />}
                            prevArrow={<SamplePrevArrow />}
                            arrows
                        >
                            {product.images.map((item) => (
                                <img
                                    className="image"
                                    src={`/uploads/${item}`}
                                    key={item}
                                    alt={item}
                                />
                            ))}
                        </Carousel>
                    ) : product.images.length === 1 ? (
                        <img
                            className="single-image"
                            src={`/uploads/${product.images[0]}`}
                            key={product.images[0]}
                            alt={product.images[0]}
                        />
                    ) : (
                        <Tag style={{ marginBottom: 15 }}>Нет изображений</Tag>
                    )}

                    <LabeledValue label="Название" value={product.name} />
                    <LabeledValue label="Цена" value={convertPrice(product.price) + ' ₽'} />
                    <LabeledValue
                        label="Категория"
                        value={product.category && product.category.name}
                    />
                    <LabeledValue
                        label="Подкатегория"
                        value={product.subcategory && product.subcategory.name}
                    />
                    <LabeledValue
                        label="Дата создания"
                        value={moment(product.createdAt).format('DD.MM.YYYY HH:mm')}
                    />
                    <LabeledValue
                        label="В архиве"
                        value={
                            product.archive ? (
                                <Tag color="red">Да</Tag>
                            ) : (
                                <Tag color="blue">Нет</Tag>
                            )
                        }
                    />
                    <LabeledValue label="Описание" value={product.description} />
                    <Actions>
                        <Link to={`/products/${id}/edit`}>
                            <Button type="primary" ghost>
                                Редактировать
                            </Button>
                        </Link>
                        <Popconfirm title="Удалить?" onConfirm={handleDelete}>
                            <Button
                                style={{ marginLeft: 10 }}
                                loading={deleting}
                                type="danger"
                                ghost
                            >
                                Удалить
                            </Button>
                        </Popconfirm>
                    </Actions>
                </>
            )}
        </View>
    )
}

export default SingleProduct
