import React, { useState } from 'react'
import styled from 'styled-components'
import { Form as AntForm, Input, InputNumber, Select, Button, Switch, Upload, message } from 'antd'
import { useQuery, useMutation } from '@apollo/react-hooks'
import { UploadOutlined } from '@ant-design/icons'

import Top from '../../components/Top'
import { GET_CATEGORIES } from '../../gqls/category/queries'
import { GET_SHOP } from '../../gqls/shop/queries'
import { EDIT_PRODUCT } from '../../gqls/product/mutations'
import { GET_PRODUCT } from '../../gqls/product/queries'

const Form = styled(AntForm)`
    max-width: 500px;

    .ant-form-item-extra {
        margin-top: 5px;
    }
`

const UploadButton = styled.div`
    display: flex;
    flex-direction: column;
    align-items: center;
    justify-content: center;

    .text {
        margin-top: 5px;
    }
`

const rules = {
    required: {
        required: true,
        message: 'Обязательное поле'
    }
}

const EditProduct = ({ history, match }) => {
    const { id } = match.params
    const [form] = Form.useForm()
    const [deletedImages, setDeletedImages] = useState([])
    const [subcategories, setSubcategories] = useState([])

    const {
        data: { shop }
    } = useQuery(GET_SHOP)
    const { data: categoriesData, loading: gettingCategories } = useQuery(GET_CATEGORIES, {
        variables: { shopId: shop.id }
    })
    const categories = categoriesData ? categoriesData.categories : []
    const { loading: productLoading } = useQuery(GET_PRODUCT, {
        variables: { id: parseInt(id) },
        onCompleted: ({ product }) => {
            const images = product.images
                ? product.images.map((img) => {
                      return {
                          uid: img,
                          name: img,
                          status: 'done',
                          url: `/uploads/${img}`,
                          uploaded: true
                      }
                  })
                : []
            form.setFieldsValue({
                name: product.name,
                description: product.description,
                price: product.price,
                archive: product.archive,
                categoryId: product.category ? product.category.id : null,
                subcategoryId: product.subcategory ? product.subcategory.id : null,
                images
            })
            if (product.category) {
                const filtered = categories.filter((item) => item.id === product.category.id)
                if (filtered[0]) {
                    setSubcategories(filtered[0].subcategories)
                }
            }
        }
    })
    const [editProduct, { loading: editing }] = useMutation(EDIT_PRODUCT, {
        onError: (err) => {
            console.error(err)
            message.error('Не удалось изменить товар')
        },
        onCompleted: () => {
            message.success('Товар изменен')
            history.push(`/products/${id}`)
        }
    })

    const normFile = (e) => {
        if (Array.isArray(e)) {
            return e
        }
        return e && e.fileList
    }

    const handleSubmit = (values) => {
        const variables = {
            ...values,
            images: values.images.map((img) => (img.originFileObj ? img.originFileObj : img)),
            id: parseInt(id),
            deletedImages
        }
        editProduct({ variables })
    }

    return (
        <>
            <Top title="Изменить товар" />
            <Form onFinish={handleSubmit} form={form} name="add-product-form" layout="vertical">
                <Form.Item label="Название" name="name" rules={[rules.required]}>
                    <Input disabled={productLoading} />
                </Form.Item>
                <Form.Item label="Цена ₽" name="price" rules={[rules.required]}>
                    <InputNumber
                        style={{ width: '100%' }}
                        decimalSeparator=","
                        disabled={productLoading}
                    />
                </Form.Item>
                <Form.Item label="Описание" name="description">
                    <Input.TextArea disabled={productLoading} autoSize={{ minRows: 4 }} />
                </Form.Item>
                <Form.Item label="Категория" name="categoryId">
                    <Select
                        onChange={(id) => {
                            const filtered = categories.filter((item) => item.id === id)
                            if (filtered[0]) {
                                setSubcategories(filtered[0].subcategories)
                            } else {
                                setSubcategories([])
                            }
                        }}
                        disabled={gettingCategories || productLoading}
                    >
                        {categories.map((item) => (
                            <Select.Option key={item.id} value={item.id}>
                                {item.name}
                            </Select.Option>
                        ))}
                    </Select>
                </Form.Item>
                <Form.Item label="Подкатегория" name="subcategoryId">
                    <Select disabled={gettingCategories}>
                        {subcategories.map((item) => (
                            <Select.Option key={item.id} value={item.id}>
                                {item.name}
                            </Select.Option>
                        ))}
                    </Select>
                </Form.Item>
                <Form.Item label="В архиве" valuePropName="checked" name="archive">
                    <Switch disabled={gettingCategories} />
                </Form.Item>
                <Form.Item
                    name="images"
                    label="Изображения"
                    valuePropName="fileList"
                    getValueFromEvent={normFile}
                    extra={
                        <span>
                            Чтобы изображения быстро загружались и ваша страница не тормозила, нужно
                            загрузить изображения не более 200 - 300 кб. Можно воспользоваться
                            онлайн сервисом для компрессии изображений{' '}
                            <a
                                href="https://imagecompressor.com"
                                target="_blank"
                                rel="noopener noreferrer"
                            >
                                https://imagecompressor.com
                            </a>
                        </span>
                    }
                    disabled={productLoading}
                >
                    <Upload
                        onChange={({ file }) => {
                            if (file.status === 'removed') {
                                setDeletedImages((prev) => [...prev, file.name])
                            }
                        }}
                        accept="image/*"
                        listType="picture-card"
                        multiple
                        beforeUpload={() => false}
                    >
                        <UploadButton>
                            <UploadOutlined />
                            <div className="text">Загрузить</div>
                        </UploadButton>
                    </Upload>
                </Form.Item>
                <Button
                    disabled={productLoading}
                    loading={editing}
                    htmlType="submit"
                    type="primary"
                >
                    Отправить
                </Button>
            </Form>
        </>
    )
}

export default EditProduct
