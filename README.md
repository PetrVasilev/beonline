File .env

```
ADMIN_SECRET="admin_secret"
ADMIN_LOGIN="admin"
ADMIN_PASSWORD="admin"
SHOP_SECRET="shop_secret"
SHOP_DOMAIN_NAME="http://localhost:8000"
CLIENT_DOMAIN_NAME="http://localhost:3001"
MAILER_EMAIL=""
MAILER_PASSWORD=""
```

File prisma/.env

```
DATABASE_URL="postgresql://postgres:postgres@localhost:5432/beonline?schema=public"
POSTGRES_USER="postgres"
POSTGRES_PASSWORD="postgres"
POSTGRES_DB="beonline"
```
