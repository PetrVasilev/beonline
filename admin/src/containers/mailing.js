import React from 'react'
import styled from 'styled-components'
import { Form as AntForm, Button, Input, message } from 'antd'
import { useMutation } from '@apollo/react-hooks'

import Top from '../components/Top'
import { MAKE_MAILING } from '../gqls/admin/mutations'

const Form = styled(AntForm)`
    max-width: 500px;
`

const rules = {
    required: {
        required: true,
        message: 'Обязательное поле'
    }
}

const Mailing = () => {
    const [makeMailing, { loading }] = useMutation(MAKE_MAILING, {
        onCompleted: () => {
            message.success('Рассылка отправлена')
        },
        onError: (err) => {
            console.error(err)
            message.error('Не удалось отправить рассылку')
        }
    })

    const handleSubmit = (values) => {
        makeMailing({
            variables: values
        })
    }

    return (
        <>
            <Top title="Рассылка" />

            <Form onFinish={handleSubmit} name="mailing-form" layout="vertical">
                <Form.Item rules={[rules.required]} label="Заголовок" name="title">
                    <Input />
                </Form.Item>
                <Form.Item rules={[rules.required]} label="Текст рассылки" name="description">
                    <Input.TextArea rows={4} />
                </Form.Item>

                <Button loading={loading} htmlType="submit" type="primary">
                    Отправить
                </Button>
            </Form>
        </>
    )
}

export default Mailing
