import React from 'react'
import styled from 'styled-components'
import { Form as AntForm, Input, Button, message } from 'antd'
import { useMutation } from '@apollo/react-hooks'

import Top from '../../components/Top'
import { CREATE_SHOP } from '../../gqls/shop/mutations'

const Form = styled(AntForm)`
    max-width: 500px;
`

const rules = {
    required: {
        required: true,
        message: 'Обязательное поле'
    },
    email: {
        type: 'email',
        message: 'Введите правильную почту'
    }
}

const ShopsAdd = ({ history }) => {
    const [createShop, { loading }] = useMutation(CREATE_SHOP, {
        onCompleted: () => {
            history.push('/shops')
            message.success('Магазин добавлен')
        },
        onError: (err) => {
            console.error(err)
            if (err.graphQLErrors) {
                err.graphQLErrors.forEach((error) => {
                    if (error.message === 'already-exist') {
                        message.error('Магазин с такой электронной почтой уже существует')
                    } else {
                        message.error('Не удалось добавить магазин')
                    }
                })
            } else {
                message.error('Не удалось добавить магазин')
            }
        }
    })

    const handleSubmit = (values) => {
        createShop({
            variables: values
        })
    }

    return (
        <>
            <Top title="Добавить магазин" />
            <Form onFinish={handleSubmit} name="add-shop-form" layout="vertical">
                <Form.Item name="name" rules={[rules.required]} label="Название">
                    <Input />
                </Form.Item>
                <Form.Item name="email" rules={[rules.required, rules.email]} label="Email">
                    <Input />
                </Form.Item>
                <Form.Item
                    name="contactsForAdmin"
                    label="Контакты"
                    extra="Контакты только для администратора"
                >
                    <Input />
                </Form.Item>
                <Button loading={loading} type="primary" htmlType="submit">
                    Отправить
                </Button>
            </Form>
        </>
    )
}

export default ShopsAdd
