import React from 'react'
import styled from 'styled-components'
import moment from 'moment'
import { Link } from 'react-router-dom'
import { useQuery, useMutation } from '@apollo/react-hooks'
import { Table as AntTable, Switch, Button, Popconfirm, message } from 'antd'

import Top from '../../components/Top'
import LabeledValue from '../../components/LabeledValue'
import { GET_SHOPS } from '../../gqls/shop/queries'
import { SET_ACCESS_SHOP, DELETE_SHOP } from '../../gqls/shop/mutations'
import { getAccessView, DOMAIN } from '../../utils'

const Table = styled(AntTable)``

const ShopInfo = styled.div`
    img {
        object-fit: cover;
        height: 100px;
        width: 150px;
    }
`

const Shop = ({ shop, refetch }) => {
    const [setAccessShop, { loading }] = useMutation(SET_ACCESS_SHOP, {
        onCompleted: () => message.success('Доступ изменен'),
        onError: (err) => {
            console.error(err)
            message.error('Не удалось изменить доступ')
        }
    })
    const [deleteShop, { loading: deleting }] = useMutation(DELETE_SHOP, {
        onCompleted: () => {
            message.success('Магазин удален')
            refetch()
        },
        onError: (err) => {
            console.error(err)
            message.error('Не удалось удалить магазин')
        }
    })

    const handleChangeAccess = (value) => {
        setAccessShop({
            variables: {
                id: shop.id,
                access: value ? 1 : 0
            }
        })
    }

    const handleDelete = () => {
        deleteShop({
            variables: {
                id: shop.id
            }
        })
    }

    const url = shop.url ? shop.url : shop.id

    return (
        <ShopInfo>
            <LabeledValue
                label="Логотип"
                value={
                    <img
                        src={shop.logo ? `/uploads/${shop.logo}` : '/default-image.jpg'}
                        alt="logo"
                    />
                }
            />
            <LabeledValue label="Описание" value={shop.description} />
            <LabeledValue
                label="Ссылка"
                value={
                    <a target="_blank" rel="noreferrer noopener" href={`${DOMAIN}/${url}`}>
                        {`${DOMAIN}/${url}`}
                    </a>
                }
            />
            <LabeledValue label="Контакты для администратора" value={shop.contactsForAdmin} />
            <LabeledValue label="Контакты для пользователя" value={shop.contactsForUsers} />
            <LabeledValue
                label="Доступ"
                value={
                    <Switch
                        loading={loading}
                        onChange={handleChangeAccess}
                        defaultChecked={shop.access === 1 ? true : false}
                    />
                }
            />
            <Popconfirm
                title="Удалить? Все данные связанные с этим магазином будут удалены"
                onConfirm={handleDelete}
            >
                <Button style={{ marginTop: 10 }} loading={deleting} type="danger" ghost>
                    Удалить
                </Button>
            </Popconfirm>
        </ShopInfo>
    )
}

const Shops = () => {
    const { data, loading, refetch } = useQuery(GET_SHOPS, {
        fetchPolicy: 'network-only'
    })

    const shops = data ? data.shops : []

    return (
        <>
            <Top title="Магазины" action={<Link to="/shops/add">+ Добавить</Link>} />
            <Table
                dataSource={shops}
                columns={columns}
                loading={loading}
                rowKey={(obj) => obj.id}
                size={window.innerWidth < 500 ? 'small' : 'default'}
                scroll={{ x: 800 }}
                expandable={{
                    expandedRowRender: (record) => <Shop refetch={refetch} shop={record} />
                }}
            />
        </>
    )
}

const columns = [
    {
        title: 'Название',
        dataIndex: 'name',
        key: 'name'
    },
    {
        title: 'Email',
        dataIndex: 'email',
        key: 'email'
    },
    {
        title: 'Дата создания',
        dataIndex: 'createdAt',
        key: 'createdAt',
        render: (createdAt) => moment(createdAt).format('DD.MM.YYYY HH:mm')
    },
    {
        title: 'Доступ',
        dataIndex: 'access',
        key: 'access',
        render: (access) => getAccessView(access)
    }
]

export default Shops
