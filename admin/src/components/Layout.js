import React from 'react'
import styled from 'styled-components'
import { Spin, Layout as AntLayout } from 'antd'
import { useQuery } from '@apollo/react-hooks'
import { Redirect } from 'react-router-dom'

import Header from './Header'
import Menu from './Menu'
import { GET_ADMIN } from '../gqls/admin/queries'

const { Content: AntContent, Footer: AntFooter, Sider: AntSider } = AntLayout

const Layout = ({ children, ...props }) => {
    const { loading, error, data } = useQuery(GET_ADMIN)

    if (loading) {
        return (
            <LoadingView>
                <Spin />
            </LoadingView>
        )
    }

    if (error) {
        return <Redirect to="/login" />
    }

    const { admin } = data

    return (
        <Provider>
            <Sider breakpoint="lg" collapsedWidth="0">
                <div onClick={() => window.open('/', '_self')} className="logo">
                    {/* <img src="/logo.png" alt="Delivery" /> */}
                    Beonline Admin
                </div>
                <Menu />
            </Sider>
            <AntLayout>
                <Header admin={admin} {...props} />
                <Content>
                    <div className="inside">{children}</div>
                </Content>
                <Footer>Панель администратора Beonline @2020</Footer>
            </AntLayout>
        </Provider>
    )
}

const withLayout = (props, Component) => (
    <Layout {...props}>
        <Component {...props} />
    </Layout>
)

const LoadingView = styled.div`
    background: whitesmoke;
    height: 100vh;
    width: 100vw;
    display: flex;
    align-items: center;
    justify-content: center;
`

const Content = styled(AntContent)`
    margin: 24px 16px 0;

    @media only screen and (max-width: 480px) {
        margin: 10px 0;
    }

    .inside {
        padding: 24px;
        background: #fff;
        min-height: 100%;

        @media only screen and (max-width: 480px) {
            padding: 14px;
        }
    }
`

const Footer = styled(AntFooter)`
    text-align: center;
`

const Provider = styled(AntLayout)`
    min-height: 100vh;

    .logo {
        height: 64px;
        display: flex;
        align-items: center;
        justify-content: center;
        font-size: 18px;
        background: whitesmoke;
        cursor: pointer;
        transition: 0.3s background;

        &:hover {
            background: #aed7ff;
        }

        img {
            width: 60%;
        }
    }

    .ant-layout-sider-zero-width-trigger {
        top: 11px;
    }
`

const Sider = styled(AntSider)`
    @media only screen and (max-width: 992px) {
        position: absolute;
        height: 100%;
    }
`

export default withLayout
