import gql from 'graphql-tag'

export const GET_SHOPS = gql`
    {
        shops {
            id
            email
            createdAt
            name
            description
            contactsForAdmin
            contactsForUsers
            logo
            access
            url
        }
    }
`
