import gql from 'graphql-tag'

export const CREATE_SHOP = gql`
    mutation($email: String!, $name: String!, $description: String, $contactsForAdmin: String) {
        createShop(
            data: {
                email: $email
                name: $name
                description: $description
                contactsForAdmin: $contactsForAdmin
            }
        ) {
            id
            email
            createdAt
            name
            description
            contactsForAdmin
            contactsForUsers
            logo
            access
            url
        }
    }
`

export const DELETE_SHOP = gql`
    mutation($id: Int!) {
        deleteShop(where: { id: $id }) {
            id
            email
            createdAt
            name
            description
            contactsForAdmin
            contactsForUsers
            logo
            access
            url
        }
    }
`

export const SET_ACCESS_SHOP = gql`
    mutation($id: Int!, $access: Int!) {
        setAccessShop(where: { id: $id }, data: { access: $access }) {
            id
            email
            createdAt
            name
            description
            contactsForAdmin
            contactsForUsers
            logo
            access
            url
        }
    }
`
