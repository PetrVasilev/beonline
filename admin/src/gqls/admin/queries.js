import gql from 'graphql-tag'

export const GET_ADMIN = gql`
    {
        admin {
            id
            login
        }
    }
`
