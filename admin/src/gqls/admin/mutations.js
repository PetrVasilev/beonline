import gql from 'graphql-tag'

export const LOGIN_ADMIN = gql`
    mutation($login: String!, $password: String!) {
        loginAdmin(data: { login: $login, password: $password }) {
            token
            admin {
                id
                login
            }
        }
    }
`

export const EDIT_PASSWORD = gql`
    mutation($currentPassword: String!, $newPassword: String!) {
        editPasswordAdmin(data: { currentPassword: $currentPassword, newPassword: $newPassword }) {
            id
            login
        }
    }
`

export const MAKE_MAILING = gql`
    mutation($title: String!, $description: String!) {
        mailing(data: { title: $title, description: $description })
    }
`
