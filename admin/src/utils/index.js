// utils here
import React from 'react'
import { Tag } from 'antd'

export const getAccessView = (access) => {
    switch (access) {
        case 0:
            return (
                <Tag style={{ minWidth: 90, textAlign: 'center' }} color="red">
                    Нет доступа
                </Tag>
            )
        case 1:
            return (
                <Tag style={{ minWidth: 90, textAlign: 'center' }} color="green">
                    Есть доступ
                </Tag>
            )
        default:
            return null
    }
}

export const DOMAIN =
    process.env.NODE_ENV === 'production' ? 'https://beonline.me' : 'http://localhost:3001'
