import gql from 'graphql-tag'

export const GET_PRODUCTS_AND_CATEGORIES = gql`
    query($shopId: Int!, $categoryId: Int, $subcategoryId: Int) {
        products(
            where: {
                shopId: $shopId
                categoryId: $categoryId
                subcategoryId: $subcategoryId
                archive: false
            }
        ) {
            id
            name
            price
            description
            images
            category {
                id
                name
            }
            subcategory {
                id
                name
            }
        }

        categories(where: { shopId: $shopId }) {
            id
            name
            subcategories {
                id
                name
                sortNumber
            }
        }
    }
`

export const GET_PRODUCTS = gql`
    query($shopId: Int!, $categoryId: Int, $subcategoryId: Int, $ids: [Int]) {
        products(
            where: {
                shopId: $shopId
                categoryId: $categoryId
                subcategoryId: $subcategoryId
                ids: $ids
                archive: false
            }
        ) {
            id
            name
            price
            description
            images
            category {
                id
                name
            }
            subcategory {
                id
                name
            }
        }
    }
`
