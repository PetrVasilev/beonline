import gql from 'graphql-tag'

export const GET_PRODUCT_AND_SHOP = gql`
    query($id: Int!, $url: String!) {
        product(where: { id: $id }) {
            id
            name
            price
            description
            images
            category {
                id
                name
            }
            subcategory {
                id
                name
            }
        }

        shopInfo(where: { url: $url }) {
            id
            name
            description
            contactsForUsers
            contactsWhatsapp
            logo
            url
            delivery
        }
    }
`
