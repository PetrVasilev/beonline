import gql from 'graphql-tag'

export const GET_SHOP = gql`
    query($url: String!) {
        shopInfo(where: { url: $url }) {
            id
            name
            description
            contactsForUsers
            contactsWhatsapp
            logo
            url
            delivery
        }
    }
`
