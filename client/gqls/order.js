import gql from 'graphql-tag'

export const CREATE_ORDER = gql`
    mutation(
        $userName: String!
        $userPhone: String!
        $userAddress: String
        $userEmail: String
        $cart: [Json!]!
        $shopId: Int!
    ) {
        createOrder(
            data: {
                userName: $userName
                userPhone: $userPhone
                userAddress: $userAddress
                userEmail: $userEmail
                cart: $cart
                shopId: $shopId
            }
        ) {
            id
            userName
            userPhone
            userEmail
            userAddress
            status
            createdAt
            shop {
                id
                name
                url
            }
            cartItems {
                id
                count
                product {
                    id
                    name
                    price
                }
            }
        }
    }
`
