import React, { useState, useEffect, createContext } from 'react'
import cookie from 'cookie'

export const CartContext = createContext({
    ready: false,
    cart: {},
    addToCart: () => {},
    removeFromCart: () => {},
    addCountOfProduct: () => {},
    removeCountOfProduct: () => {},
    clearCart: () => {}
})

const saveCartToCookie = (cart) => {
    try {
        document.cookie = cookie.serialize('cart', JSON.stringify(cart), {
            sameSite: true,
            path: '/',
            maxAge: 30 * 24 * 60 * 60 // 30 days
        })
    } catch (err) {
        console.log(err)
    }
}

export default ({ children }) => {
    const [cart, setCart] = useState({})
    const [ready, setReady] = useState(false)

    useEffect(() => {
        const parsedCookie = cookie.parse(document.cookie)
        let cart
        try {
            cart = JSON.parse(parsedCookie.cart)
        } catch (err) {
            cart = {}
        }
        setCart(cart)
        setReady(true)
    }, [])

    const addToCart = (shopId, productId) => {
        setCart((prev) => {
            const previous = { ...prev }
            if (previous[shopId]) {
                previous[shopId].push({
                    id: productId,
                    count: 1
                })
            } else {
                previous[shopId] = [
                    {
                        id: productId,
                        count: 1
                    }
                ]
            }
            saveCartToCookie(previous)
            return previous
        })
    }

    const removeFromCart = (shopId, productId) => {
        setCart((prev) => {
            const previous = { ...prev }
            previous[shopId] = previous[shopId].filter((item) => item.id !== productId)
            saveCartToCookie(previous)
            return previous
        })
    }

    const addCountOfProduct = (shopId, productId) => {
        setCart((prev) => {
            const previous = { ...prev }
            previous[shopId] = previous[shopId].map((item) => {
                if (item.id === productId) {
                    item.count += 1
                }
                return item
            })
            saveCartToCookie(previous)
            return previous
        })
    }

    const clearCart = () => {
        setCart({})
        saveCartToCookie({})
    }

    const removeCountOfProduct = (shopId, productId) => {
        setCart((prev) => {
            const previous = { ...prev }
            previous[shopId] = previous[shopId].map((item) => {
                if (item.id === productId) {
                    if (item.count > 1) {
                        item.count -= 1
                    }
                }
                return item
            })
            saveCartToCookie(previous)
            return previous
        })
    }

    return (
        <CartContext.Provider
            value={{
                cart,
                addToCart,
                removeFromCart,
                addCountOfProduct,
                removeCountOfProduct,
                clearCart,
                ready
            }}
        >
            {children}
        </CartContext.Provider>
    )
}
