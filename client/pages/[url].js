import React from 'react'
import styled from 'styled-components'
import { useQuery } from '@apollo/react-hooks'

import Page404 from './404'
import { GET_PRODUCTS_AND_CATEGORIES } from '../gqls'
import { GET_SHOP } from '../gqls/shop'
import { withApollo } from '../utils/apollo'
import Container from '../components/Container'
import CustomHead from '../components/CustomHead'
import ShopInfo from '../components/ShopInfo'
import Products from '../components/Products'
import CartButton from '../components/CartButton'

const View = styled.div`
    padding-top: 40px;
    margin-bottom: 50px;

    @media only screen and (max-width: 500px) {
        padding-top: 20px;
    }
`

const Shop = ({ shop }) => {
    const data = useQuery(GET_PRODUCTS_AND_CATEGORIES, {
        variables: {
            shopId: shop ? shop.id : null
        }
    })

    if (!shop) return <Page404 />

    return (
        <>
            <CustomHead title={shop.name} logo={shop.logo} description={shop.description} />
            <Container>
                <View>
                    <ShopInfo shop={shop} />
                    <Products data={data} shop={shop} />
                </View>
            </Container>
            <CartButton shop={shop} />
        </>
    )
}

Shop.getInitialProps = async ({ apolloClient, ...props }) => {
    try {
        const { url } = props.query
        const { data } = await apolloClient.query({
            query: GET_SHOP,
            variables: {
                url
            }
        })
        return {
            shop: data.shopInfo
        }
    } catch (err) {
        return {}
    }
}

export default withApollo({ ssr: true })(Shop)
