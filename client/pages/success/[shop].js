import React from 'react'
import styled from 'styled-components'
import Router from 'next/router'

import CheckSVG from '../../public/icons/check.svg'
import Container from '../../components/Container'
import Button from '../../components/Button'
import CustomHead from '../../components/CustomHead'
import { GET_SHOP } from '../../gqls/shop'
import { withApollo } from '../../utils/apollo'
import Page404 from '../404'

const View = styled.div`
    padding: 40px 0;

    display: flex;
    flex-direction: column;
    align-items: center;
    justify-content: center;

    .icon {
        width: 100px;
        height: 100px;

        * {
            stroke: #0cd40c;
        }
    }

    .title {
        font-size: 18px;
        color: rgba(0, 0, 0, 0.7);
        margin-top: 10px;
    }

    .text {
        font-size: 14px;
        color: rgba(0, 0, 0, 0.7);
        margin-top: 5px;
    }

    .shop-button {
        margin-top: 20px;
    }
`

const Success = ({ shop }) => {
    if (!shop) return <Page404 />

    return (
        <>
            <CustomHead title={shop.name} logo={shop.logo} description={shop.description} />
            <Container>
                <View>
                    <CheckSVG className="icon" />
                    <div className="title">Заказ успешно создан :)</div>
                    <div className="text">Скоро менеджер магазина свяжется с вами</div>
                    <Button
                        onClick={() => {
                            Router.replace('/[url]', `/${shop.url ? shop.url : shop.id}`)
                        }}
                        className="shop-button"
                    >
                        Обратно в магазин
                    </Button>
                </View>
            </Container>
        </>
    )
}

Success.getInitialProps = async ({ apolloClient, ...props }) => {
    try {
        const { shop } = props.query
        const { data } = await apolloClient.query({
            query: GET_SHOP,
            variables: {
                url: shop
            }
        })
        return {
            shop: data.shopInfo
        }
    } catch (err) {
        return {}
    }
}

export default withApollo({ ssr: true })(Success)
