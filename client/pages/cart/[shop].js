import React, { useContext } from 'react'
import styled from 'styled-components'
import { useQuery } from '@apollo/react-hooks'

import Page404 from '../404'
import { withApollo } from '../../utils/apollo'
import { GET_PRODUCTS } from '../../gqls'
import { GET_SHOP } from '../../gqls/shop'
import { CartContext } from '../../context/cart'
import Container from '../../components/Container'
import CustomHead from '../../components/CustomHead'
import CartComponent from '../../components/Cart'
import BackButton from '../../components/BackButton'

const View = styled.div`
    padding: 40px 0;

    @media only screen and (max-width: 500px) {
        padding-top: 20px;
        padding-bottom: 0;
    }
`

const Cart = ({ shop }) => {
    const { cart } = useContext(CartContext)

    const cartData = useQuery(GET_PRODUCTS, {
        variables: {
            shopId: shop ? shop.id : null,
            ids: shop ? (cart[shop.id] ? cart[shop.id].map((item) => item.id) : []) : []
        }
    })

    if (!shop) return <Page404 />

    return (
        <>
            <CustomHead title={`Корзина ${shop.name}`} logo={shop.logo} />
            <Container>
                <View>
                    <BackButton />
                    <CartComponent data={cartData} shop={shop} />
                </View>
            </Container>
        </>
    )
}

Cart.getInitialProps = async ({ apolloClient, req, ...props }) => {
    try {
        const { shop } = props.query
        const { data: shopData } = await apolloClient.query({
            query: GET_SHOP,
            variables: {
                url: shop
            }
        })
        return {
            shop: shopData.shopInfo
        }
    } catch (err) {
        return {}
    }
}

export default withApollo({ ssr: true })(Cart)
