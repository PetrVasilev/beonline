import App from 'next/app'
import React from 'react'
import 'slick-carousel/slick/slick.css'
import 'slick-carousel/slick/slick-theme.css'

import GlobalStyles from '../components/GlobalStyles'
import CartContextProvider from '../context/cart'

export default class MyApp extends App {
    render() {
        const { Component, pageProps } = this.props
        return (
            <CartContextProvider>
                <Component {...pageProps} />
                <GlobalStyles />
            </CartContextProvider>
        )
    }
}
