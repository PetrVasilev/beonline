import React from 'react'
import styled from 'styled-components'
import Head from 'next/head'

const View = styled.div`
    padding: 30px 10px;
    display: flex;
    flex-direction: column;
    align-items: center;
    justify-content: center;

    img {
        width: 250px;
        height: 250px;
        margin-bottom: 20px;
    }

    .info {
        font-size: 20px;
        text-align: center;
        color: gray;
        padding: 0 35vw;

        @media only screen and (max-width: 700px) {
            padding: 0 15px;
        }
    }

    .email {
        margin-top: 10px;
        font-size: 16px;
        color: gray;
    }
`

const Main = () => {
    return (
        <>
            <Head>
                <meta name="description" content="" />
                <link href="/static/favicon.ico" rel="shortcut icon" type="image/x-icon" />
                <title>BeOnline</title>
            </Head>
            <View>
                <img src={`/static/logos/logo2-min.png`} alt="Beonline" />
                <div className="info">
                    Сервис для создания интернет магазина для вашей страницы Instagram
                </div>
                <div className="email">
                    Свяжитесь с нами <a href="mailto:support@beonline.me">support@beonline.me</a>
                </div>
            </View>
        </>
    )
}

export default Main
