import React from 'react'
import styled from 'styled-components'

import Container from '../components/Container'
import CustomHead from '../components/CustomHead'
import ProductInfo from '../components/ProductInfo'
import CartButton from '../components/CartButton'
import AddToCartButton from '../components/AddToCartButton'
import BackButton from '../components/BackButton'
import { withApollo } from '../utils/apollo'
import { GET_PRODUCT_AND_SHOP } from '../gqls/product'
import Page404 from './404'

const View = styled.div`
    padding-top: 40px;
    margin-bottom: 95px;

    @media only screen and (max-width: 500px) {
        padding-top: 20px;
    }
`

const Product = ({ product, shop }) => {
    if (!product || !shop) return <Page404 />
    return (
        <>
            <CustomHead title={product.name} logo={shop.logo} description={product.description} />
            <Container>
                <View>
                    <BackButton />
                    <ProductInfo product={product} shop={shop} />
                </View>
            </Container>
            <AddToCartButton shop={shop} product={product} />
            <CartButton shop={shop} />
        </>
    )
}

Product.getInitialProps = async ({ query, apolloClient }) => {
    const { product } = query
    if (product.length !== 2) return {}
    const shopUrl = product[0]
    const productId = product[1]
    if (productId.split('.').length > 1) return {}
    try {
        const { data } = await apolloClient.query({
            query: GET_PRODUCT_AND_SHOP,
            variables: { id: parseInt(productId), url: shopUrl }
        })
        return {
            product: data ? data.product : null,
            shop: data ? data.shopInfo : null
        }
    } catch (err) {
        return {}
    }
}

export default withApollo({ ssr: true })(Product)
