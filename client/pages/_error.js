import React from 'react'
import styled from 'styled-components'
import Head from 'next/head'

import Container from '../components/Container'
import AlertSVG from '../public/icons/alert.svg'

const View = styled.div`
    display: flex;
    align-items: center;
    justify-content: center;
    flex-direction: column;
    height: 70vh;
    box-sizing: border-box;

    .icon {
        width: 50px;
        height: 50px;
        margin-bottom: 15px;

        * {
            stroke: gray;
        }
    }

    .text {
        font-size: 25px;
        color: gray;

        @media only screen and (max-width: 500px) {
            font-size: 18px;
        }
    }
`

const Page404 = () => {
    return (
        <>
            <Head>
                <meta name="description" content="Not found page" />
                <link href="/static/favicon.ico" rel="shortcut icon" type="image/x-icon" />
                <title>Ошибка на сервере | BeOnline</title>
            </Head>
            <Container>
                <View>
                    <AlertSVG className="icon" />
                    <div className="text">Ошибка на сервере</div>
                </View>
            </Container>
        </>
    )
}

export default Page404
