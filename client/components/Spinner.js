import React from 'react'
import styled from 'styled-components'

const View = styled.div`
    .loader {
        border: ${(props) => props.size / props.separator}px solid rgba(0, 0, 0, 0.1);
        border-radius: 50%;
        border-top: ${(props) => props.size / props.separator}px solid ${(props) => props.color};
        width: ${(props) => props.size}px;
        height: ${(props) => props.size}px;
        -webkit-animation: spin 0.8s linear infinite;
        animation: spin 0.8s linear infinite;
    }

    @-webkit-keyframes spin {
        0% {
            -webkit-transform: rotate(0deg);
        }
        100% {
            -webkit-transform: rotate(360deg);
        }
    }

    @keyframes spin {
        0% {
            transform: rotate(0deg);
        }
        100% {
            transform: rotate(360deg);
        }
    }
`

const Spinner = ({ size = 24, separator = 6, color = '#0070da' }) => {
    return (
        <View size={size} separator={separator} color={color}>
            <div className="loader"></div>
        </View>
    )
}

export default Spinner
