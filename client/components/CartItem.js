import React from 'react'
import styled from 'styled-components'

import Button from './Button'
import AddSVG from '../public/icons/add.svg'
import RemoveSVG from '../public/icons/remove.svg'
import { UploadsURL, convertPrice } from '../utils'

const View = styled.div`
    margin-bottom: 10px;
    display: flex;
    flex-direction: row;

    .image-background {
        height: 7.3rem;
        width: 7.3rem;
        min-width: 7.3rem;
        min-height: 7.3rem;
        position: relative;
        overflow: hidden;
        border: 1px solid #cccccc;
        margin-right: 20px;

        .filter {
            height: 7.3rem;
            width: 7.3rem;
            background-position: center;
            background-size: cover;
            filter: blur(8px);
            -webkit-filter: blur(8px);
            position: absolute;
            top: 0;
            right: 0;
            bottom: 0;
            left: 0;
            z-index: 0;
        }

        .image {
            object-fit: contain;
            height: 7.3rem;
            width: 7.3rem;
            position: absolute;
            top: 0;
            right: 0;
            bottom: 0;
            left: 0;
            z-index: 1;
        }

        @media only screen and (max-width: 500px) {
            height: 27vw;
            width: 27vw;
            min-width: 27vw;
            min-height: 27vw;

            .filter {
                height: 27vw;
                width: 27vw;
            }

            .image {
                height: 27vw;
                width: 27vw;
            }
        }

        @media only screen and (max-width: 375px) {
            height: 28vw;
            width: 28vw;
            min-width: 28vw;
            min-height: 28vw;

            .filter {
                height: 28vw;
                width: 28vw;
            }

            .image {
                height: 28vw;
                width: 28vw;
            }
        }
    }

    .info {
        width: 100%;

        @media only screen and (max-width: 500px) {
            margin-left: 13px;
        }

        .top {
            .name {
                font-size: 18px;

                @media only screen and (max-width: 500px) {
                    font-size: 16px;
                }

                @media only screen and (max-width: 400px) {
                    font-size: 14px;
                }
            }

            .category {
                font-size: 14px;
                margin-top: 5px;
                color: gray;

                @media only screen and (max-width: 500px) {
                    font-size: 12px;
                }
            }

            .price {
                font-size: 18px;
                font-weight: 500;
                margin-top: 3px;

                @media only screen and (max-width: 500px) {
                    font-size: 16px;
                }

                @media only screen and (max-width: 400px) {
                    font-size: 14px;
                }
            }
        }

        .bottom {
            margin-top: 10px;
            margin-bottom: 5px;
            display: flex;
            flex-direction: row;
            align-items: center;
            justify-content: space-between;

            @media only screen and (max-width: 375px) {
                margin-top: 5px;

                button {
                    font-size: 10px;
                }
            }

            .counter {
                display: flex;
                flex-direction: row;
                align-items: center;
                justify-content: space-between;

                .knock {
                    width: 25px;
                    height: 25px;
                    cursor: pointer;

                    svg {
                        width: 25px;
                        height: 25px;
                    }

                    @media only screen and (max-width: 375px) {
                        width: 22px;
                        height: 22px;

                        svg {
                            width: 22px;
                            height: 22px;
                        }
                    }
                }

                .value {
                    font-size: 16px;
                    margin: 0 10px;

                    @media only screen and (max-width: 340px) {
                        font-size: 14px;
                    }
                }
            }
        }
    }
`

const CartItem = ({
    item,
    cart,
    shop,
    firstImage,
    addCountOfProduct,
    removeCountOfProduct,
    removeFromCart
}) => {
    if (!cart[shop.id]) return null
    const index = cart[shop.id].findIndex((it) => it.id === item.id)
    if (index < 0) return null
    const count = cart[shop.id][index].count
    return (
        <View>
            {firstImage && (
                <div className="image-background">
                    <div
                        className="filter"
                        style={
                            firstImage
                                ? {
                                      backgroundImage: `url('${UploadsURL}/uploads/${firstImage}')`
                                  }
                                : { backgroundColor: '#c3c3c3' }
                        }
                    />
                    <img
                        id={item.id}
                        src={
                            firstImage
                                ? `${UploadsURL}/uploads/${firstImage}`
                                : '/static/default-image.jpg'
                        }
                        className="image"
                        alt={item.name}
                    />
                </div>
            )}

            <div className="info">
                <div className="top">
                    <div className="name">{item.name}</div>
                    {item.category && (
                        <div className="category">
                            {item.category.name}
                            {item.subcategory ? `, ${item.subcategory.name}` : ''}
                        </div>
                    )}
                    <div className="price">{convertPrice(item.price)} руб.</div>
                </div>
                <div className="bottom">
                    <div className="counter">
                        <div
                            onClick={() => {
                                removeCountOfProduct(shop.id, item.id)
                            }}
                            className="knock"
                        >
                            <RemoveSVG />
                        </div>
                        <div className="value">{count}</div>
                        <div
                            onClick={() => {
                                addCountOfProduct(shop.id, item.id)
                            }}
                            className="knock"
                        >
                            <AddSVG />
                        </div>
                    </div>

                    <Button
                        onClick={() => {
                            removeFromCart(shop.id, item.id)
                        }}
                        small
                        color="#d60000"
                        ghost
                    >
                        Удалить
                    </Button>
                </div>
            </div>
        </View>
    )
}

export default CartItem
