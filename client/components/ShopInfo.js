import React from 'react'
import styled from 'styled-components'

import { UploadsURL, phoneMask } from '../utils'
import PhoneSVG from '../public/icons/call.svg'
import WhatsappSVG from '../public/icons/whatsapp.svg'

const View = styled.div`
    display: flex;
    flex-direction: row;
    padding-bottom: 30px;
    border-bottom: 1px solid rgba(var(--b38, 219, 219, 219), 1);

    @media only screen and (max-width: 500px) {
        padding-bottom: 20px;
    }
`

const Logo = styled.img`
    width: 170px;
    min-width: 170px;
    height: 170px;
    min-height: 170px;
    object-fit: cover;
    border: 1px solid #e2e2e2;
    margin-right: 50px;

    @media only screen and (max-width: 600px) {
        margin-right: 30px;
    }

    @media only screen and (max-width: 500px) {
        width: 25vw;
        min-width: 25vw;
        height: 25vw;
        min-height: 25vw;
        margin-right: 1rem;
    }
`

const Info = styled.div`
    width: 100%;

    .name {
        font-size: 28px;
        font-weight: 500;
        margin-top: 0;
        margin-bottom: 0;
        letter-spacing: 2px;

        @media only screen and (max-width: 500px) {
            font-size: 22px;
            letter-spacing: 1px;
        }
    }

    .description {
        margin-top: 12px;
        font-size: 14px;
        white-space: pre-line;
        color: rgba(0, 0, 0, 0.7);

        @media only screen and (max-width: 500px) {
            margin-top: 5px;
        }
    }

    .contacts {
        margin-top: 12px;
        display: flex;
        flex-direction: row;
        align-items: center;

        @media only screen and (max-width: 500px) {
            margin-top: 10px;
        }

        .icon {
            width: 18px;
            height: 18px;
            fill: #8e8e8e;

            @media only screen and (max-width: 500px) {
                width: 16px;
                height: 16px;
            }
        }

        .phones {
            margin-left: 8px;
            font-size: 14px;
        }
    }

    .whatsapp {
        margin-top: 5px;

        @media only screen and (max-width: 500px) {
            margin-top: 5px;
        }

        .icon {
            fill: #25d366;
            width: 20px;
            height: 20px;

            @media only screen and (max-width: 500px) {
                width: 17px;
                height: 17px;
            }
        }

        .text {
            font-size: 14px;
            margin-left: 8px;
        }
    }
`

const ShopInfo = ({ shop }) => {
    const whatsappNumber = shop.contactsWhatsapp
        ? shop.contactsWhatsapp.replace(/ /g, '').replace(/\D/g, '')
        : null

    const phones = shop.contactsForUsers ? shop.contactsForUsers.split(',') : []

    return (
        <View>
            {shop.logo && (
                <Logo src={`${UploadsURL}/uploads/${shop.logo}`} className="logo" alt={shop.name} />
            )}
            <Info>
                <h1 className="name">{shop.name}</h1>
                <div className="description">
                    {shop.description ? shop.description : 'Нет описания'}
                </div>
                {phones.length > 0 && (
                    <div className="contacts">
                        <PhoneSVG className="icon" />
                        <div className="phones">
                            {phones
                                .map((item, index) => {
                                    const masked = phoneMask(item)
                                    if (!masked) return null
                                    return (
                                        <span key={item}>
                                            {index !== 0 && (
                                                <span style={{ marginRight: 5 }}>,</span>
                                            )}
                                            <a href={`tel:${item}`}>{masked}</a>
                                        </span>
                                    )
                                })
                                .filter((item) => item)}
                        </div>
                    </div>
                )}
                {whatsappNumber && (
                    <div className="contacts whatsapp">
                        <WhatsappSVG className="icon" />
                        <a href={`https://wa.me/${whatsappNumber}`} className="text">
                            Написать в Whatsapp
                        </a>
                    </div>
                )}
            </Info>
        </View>
    )
}

export default ShopInfo
