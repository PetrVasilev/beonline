import React, { useContext } from 'react'
import styled from 'styled-components'

import Spinner from './Spinner'
import CartItem from './CartItem'
import CartSummary from './CartSummary'
import CartSVG from '../public/icons/cart.svg'
import { CartContext } from '../context/cart'

const Title = styled.div`
    font-size: 28px;
    font-weight: 500;

    @media only screen and (max-width: 500px) {
        font-size: 24px;
    }
`

const View = styled.div`
    .no-data {
        display: flex;
        align-items: center;
        justify-content: center;
        flex-direction: column;
        padding: 35px 0;

        .icon {
            width: 70px;
            height: 70px;

            * {
                stroke: gray;
            }
        }

        .text {
            font-size: 14px;
            color: gray;
            text-align: center;
            margin-top: 5px;
        }
    }
`

const Loading = styled.div`
    width: 100%;
    display: flex;
    align-items: center;
    justify-content: center;
    height: 22vh;
`

const List = styled.div`
    margin-top: 20px;
`

const Cart = ({ data: cartData, shop }) => {
    const { cart, addCountOfProduct, removeCountOfProduct, removeFromCart } = useContext(
        CartContext
    )
    const { data, loading } = cartData
    const cartProducts = data ? data.products : []
    const filtered = cartProducts.filter((item) => {
        if (!cart[shop.id]) return false
        const index = cart[shop.id].findIndex((it) => it.id === item.id)
        if (index > -1) {
            return true
        } else {
            return false
        }
    })
    return (
        <View>
            <Title>Корзина</Title>
            {loading ? (
                <Loading>
                    <Spinner />
                </Loading>
            ) : filtered.length === 0 ? (
                <div className="no-data">
                    <CartSVG className="icon" />
                    <div className="text">Корзина пуста</div>
                </div>
            ) : (
                <>
                    <List>
                        {filtered.map((item) => {
                            const firstImage = Array.isArray(item.images) ? item.images[0] : null
                            return (
                                <CartItem
                                    cart={cart}
                                    shop={shop}
                                    firstImage={firstImage}
                                    item={item}
                                    key={item.id}
                                    addCountOfProduct={addCountOfProduct}
                                    removeCountOfProduct={removeCountOfProduct}
                                    removeFromCart={removeFromCart}
                                />
                            )
                        })}
                    </List>

                    <CartSummary products={filtered} shop={shop} />
                </>
            )}
        </View>
    )
}

export default Cart
