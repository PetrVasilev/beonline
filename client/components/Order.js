import React, { useState } from 'react'
import styled from 'styled-components'
import InputMask from 'react-input-mask'
import { useMutation } from '@apollo/react-hooks'
import Router from 'next/router'

import Input from './Input'
import Button from './Button'
import { CREATE_ORDER } from '../gqls/order'

const View = styled.div`
    margin-top: 12px;
`

const Title = styled.div`
    font-size: 28px;
    font-weight: 500;

    @media only screen and (max-width: 500px) {
        font-size: 24px;
    }
`

const Form = styled.form`
    padding: 12px 0;

    .order-button {
        width: 100%;
        margin-top: 14px;
    }
`

const Error = styled.div`
    color: red;
    font-size: 14px;
`

const Order = ({ cart, shop, clearCart }) => {
    const [error, setError] = useState(null)

    const [createOrder, { loading }] = useMutation(CREATE_ORDER, {
        onCompleted: () => {
            Router.push(`/success/[shop]`, `/success/${shop.url ? shop.url : shop.id}`).then(() => {
                clearCart()
            })
        },
        onError: (err) => {
            console.error(err)
            setError('Не удалось сделать заказ :( Попробуйте еще раз')
        }
    })

    const handleSubmit = (e) => {
        e.preventDefault()
        const userName = e.target.name.value
        const userPhone = e.target.phone.value
        const userAddress = e.target.address ? e.target.address.value : undefined
        const userEmail = e.target.email.value
        const variables = {
            userName,
            userPhone,
            userAddress,
            userEmail,
            cart: cart ? (cart[shop.id] ? cart[shop.id] : []) : [],
            shopId: shop.id
        }
        setError(null)
        createOrder({
            variables
        })
    }

    return (
        <View>
            <Title>Заказать</Title>
            <Form onSubmit={handleSubmit}>
                <Input label="Имя" required name="name" placeholder="Фамилия Имя" />
                <InputMask
                    mask="+7 (999) 999 99 99"
                    name="phone"
                    required
                    placeholder="+7 (___) ___ __ __"
                >
                    {(inputProps) => <Input {...inputProps} label="Номер телефона" />}
                </InputMask>
                {shop.delivery && (
                    <Input
                        label="Адрес доставки"
                        name="address"
                        placeholder="Город, улица, дом, квартира"
                    />
                )}
                <Input
                    label="Электронный адрес"
                    type="email"
                    name="email"
                    placeholder="example@mail.ru"
                    extra="Если хотите получать информацию о заказе на электронный адрес"
                />
                {error && <Error>{error}</Error>}
                <Button loading={loading} className="order-button">
                    Заказать
                </Button>
            </Form>
        </View>
    )
}

export default Order
