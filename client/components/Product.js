import React from 'react'
import styled from 'styled-components'
import Link from 'next/link'

import { UploadsURL, convertPrice } from '../utils'

const View = styled.div`
    cursor: pointer;

    @media only screen and (max-width: 500px) {
        margin-bottom: 10px;

        &:last-child,
        :nth-last-child(2) {
            margin-bottom: 0;
        }
    }

    &:hover {
        .image {
            filter: brightness(95%);
        }
    }

    .image {
        width: 100%;
        height: 220px;
        border: 1px solid #d4d4d4;
        top: 0;
        right: 0;
        bottom: 0;
        left: 0;
        z-index: 1;
        transition: 0.2s filter;
        background-color: white;

        @media only screen and (max-width: 710px) {
            height: 250px;
        }

        @media only screen and (max-width: 450px) {
            height: 200px;
        }

        @media only screen and (max-width: 400px) {
            height: 180px;
        }

        @media only screen and (max-width: 330px) {
            height: 160px;
        }
    }

    .info {
        margin-top: 5px;

        .name {
            font-size: 14px;
        }

        .price {
            font-size: 16px;
            font-weight: 500;
            margin-top: 1px;
        }
    }
`

const Product = ({ product, shop }) => {
    const firstImage = Array.isArray(product.images) ? product.images[0] : null

    return (
        <Link href="/[...product]" as={`/${shop.url ? shop.url : shop.id}/${product.id}`}>
            <View>
                <img
                    id={product.id}
                    src={
                        firstImage
                            ? `${UploadsURL}/uploads/${firstImage}`
                            : '/static/default-image.jpg'
                    }
                    style={{ objectFit: firstImage ? 'contain' : 'cover' }}
                    className="image"
                    alt={product.name}
                />
                <div className="info">
                    <div className="name">{product.name}</div>
                    <div className="price">{convertPrice(product.price)} руб.</div>
                </div>
            </View>
        </Link>
    )
}

export default Product
