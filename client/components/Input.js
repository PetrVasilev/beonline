import React from 'react'
import styled from 'styled-components'

const View = styled.div`
    margin-bottom: 10px;

    input {
        -webkit-appearance: none;
        -moz-appearance: none;
        appearance: none;

        width: 100%;
        height: 40px;
        border: 1px solid #cccccc;
        font-size: 16px;
        padding-left: 12px;
        box-sizing: border-box;
        margin-top: 3px;
        outline: none;
        font-weight: 400;
        font-family: 'Manrope', sans-serif;
        letter-spacing: 0.2px;
    }

    label {
        font-size: 14px;
        letter-spacing: 0.3px;
    }

    .extra {
        font-size: 14px;
        margin-top: 3px;
        color: gray;
    }
`

const Input = ({ label, name, containerStyle, extra, ...props }) => {
    return (
        <View style={containerStyle}>
            {label && <label htmlFor={name}>{label}</label>}
            <input id={name} {...props} />
            {extra && <div className="extra">{extra}</div>}
        </View>
    )
}

export default Input
