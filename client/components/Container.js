import React from 'react'
import styled from 'styled-components'

const Container = styled.div`
    margin: 0 auto;
    max-width: 700px;
    box-sizing: border-box;
    padding-bottom: 23px;

    @media only screen and (max-width: 710px) {
        padding: 0 15px;
        padding-bottom: 23px;
    }
`

const MobileContainer = ({ children, ...props }) => {
    return <Container {...props}>{children}</Container>
}

export default MobileContainer
