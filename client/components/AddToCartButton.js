import React, { useContext } from 'react'
import styled from 'styled-components'

import Button from './Button'
import { CartContext } from '../context/cart'

const View = styled.div`
    position: fixed;
    bottom: 55px;
    left: 0;
    right: 0;
    z-index: 3;
    display: flex;
    align-items: center;
    justify-content: center;

    @media only screen and (max-width: 710px) {
        left: 15px;
        right: 15px;
    }

    .cart-button {
        width: 700px;

        @media only screen and (max-width: 710px) {
            width: 100%;
        }

        &:hover {
            opacity: 1;
        }
    }

    .remove-button {
        background-color: white;
        border-color: red;
        color: red;
    }
`

const checkProductInCart = (product, cart, shop) => {
    if (!cart[shop.id]) {
        return false
    }
    const index = cart[shop.id].findIndex((item) => item.id === product.id)
    if (index > -1) {
        return true
    } else {
        return false
    }
}

const AddToCartButton = ({ product, shop }) => {
    const { cart, addToCart, removeFromCart } = useContext(CartContext)
    const productInCart = checkProductInCart(product, cart, shop)
    return (
        <View>
            {productInCart ? (
                <Button
                    onClick={() => removeFromCart(shop.id, product.id)}
                    className="cart-button remove-button"
                    ghost
                >
                    Удалить из корзины
                </Button>
            ) : (
                <Button onClick={() => addToCart(shop.id, product.id)} className="cart-button">
                    Добавить в корзину
                </Button>
            )}
        </View>
    )
}

export default AddToCartButton
