import React from 'react'
import styled from 'styled-components'

import Spinner from './Spinner'

const View = styled.button`
    outline: none;
    ${(props) =>
        props.ghost
            ? `
                background: transparent;
                color: ${props.color};
            `
            : `
                background: ${props.color};
                color: white;
            `}

    border: 1px solid ${(props) => props.color};
    border-radius: 5px;
    ${(props) =>
        props.small
            ? `
                padding: 5px 12px;
                font-size: 12px;`
            : `
                padding: 10px 20px;
                font-size: 14px;`}
    box-shadow: none;
    box-sizing: border-box;
    
    display: flex;
    align-items: center;
    justify-content: center;
    text-transform: uppercase;
    cursor: pointer;
`

const Button = ({
    children,
    ghost = false,
    color = '#0070da',
    loading = false,
    small,
    ...props
}) => {
    return (
        <View ghost={ghost} small={small} color={color} {...props}>
            {loading ? (
                <Spinner size={13} separator={6.5} color={ghost ? null : 'white'} />
            ) : (
                children
            )}
        </View>
    )
}

export default Button
