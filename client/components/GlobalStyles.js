import { createGlobalStyle } from 'styled-components'

export default createGlobalStyle`
    body {
        margin: 0;
        padding: 0;
        background: whitesmoke;
        font-family: 'Manrope', sans-serif;
        overflow-y: scroll;
    }

    a {
        color: #0070da;
        text-decoration: none;

        &:hover {
            text-decoration: underline;
        }
    }
`
