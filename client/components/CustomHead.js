import React from 'react'
import Head from 'next/head'

import { UploadsURL } from '../utils'

const CustomHead = ({ title, description, logo }) => {
    return (
        <Head>
            <title>{title}</title>
            {description && <meta name="description" content={description} />}
            {logo ? (
                <link rel="icon" type="image/png" href={`${UploadsURL}/uploads/${logo}`} />
            ) : (
                <link href={'/static/favicon.ico'} rel="shortcut icon" type="image/x-icon" />
            )}
        </Head>
    )
}

export default CustomHead
