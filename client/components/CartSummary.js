import React, { useContext } from 'react'
import styled from 'styled-components'

import { CartContext } from '../context/cart'
import { convertPrice } from '../utils'
import Order from './Order'

const View = styled.div`
    margin-top: 15px;

    .text {
        font-size: 18px;
        display: flex;
        flex-direction: row;
        justify-content: flex-end;

        .total {
            font-weight: 500;
            margin-left: 10px;
        }

        .label {
            font-weight: 300;
        }
    }
`

const CartSummary = ({ products = [], shop }) => {
    const { cart, clearCart } = useContext(CartContext)

    const shopCart = cart[shop.id] ? cart[shop.id] : []

    const total = shopCart.reduce((previous, current) => {
        const index = products.findIndex((item) => item.id === current.id)
        if (index > -1) {
            return previous + current.count * products[index].price
        } else {
            return previous
        }
    }, 0)

    return (
        <View>
            <div className="text">
                <div className="label">Всего</div>
                <div className="total">{convertPrice(total)} руб.</div>
            </div>

            <Order shop={shop} clearCart={clearCart} cart={cart} />
        </View>
    )
}

export default CartSummary
