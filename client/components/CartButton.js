import React, { useContext } from 'react'
import styled from 'styled-components'
import Router from 'next/router'

import { CartContext } from '../context/cart'
import Button from './Button'

const View = styled.div`
    position: fixed;
    bottom: 10px;
    left: 0;
    right: 0;
    z-index: 3;
    display: flex;
    align-items: center;
    justify-content: center;

    @media only screen and (max-width: 710px) {
        left: 15px;
        right: 15px;
    }

    .button {
        width: 700px;

        @media only screen and (max-width: 710px) {
            width: 100%;
        }

        &:hover {
            opacity: 1;
        }
    }
`

const CartButton = ({ shop }) => {
    const { cart } = useContext(CartContext)

    const cartCount = Array.isArray(cart[shop.id]) ? cart[shop.id].length : 0

    return (
        <View>
            <Button
                onClick={() =>
                    Router.push(`/cart/[shop]`, `/cart/${shop.url ? shop.url : shop.id}`)
                }
                color="#e28c0c"
                className="button"
            >
                Корзина ({cartCount})
            </Button>
        </View>
    )
}

export default CartButton
