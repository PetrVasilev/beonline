import React from 'react'
import styled from 'styled-components'
import Slider from 'react-slick'

import { UploadsURL, convertPrice } from '../utils'
import NextArrowSVG from '../public/icons/next-arrow.svg'
import PrevArrowSVG from '../public/icons/prev-arrow.svg'

const View = styled.div`
    .arrow {
        position: absolute;
        z-index: 1;
        cursor: pointer;
        display: flex;
        align-items: center;
        justify-content: center;
        border-radius: 50%;
        width: 25px;
        height: 25px;
        background: whitesmoke;

        .icon {
            width: 23px;
            height: 23px;
            transition: 0.2s opacity;

            &:hover {
                opacity: 0.7;
            }

            path {
                stroke: rgba(0, 0, 0, 0.8);
            }

            @media only screen and (max-width: 800px) {
                width: 18px;
                height: 18px;
            }
        }
    }

    .next {
        right: -35px;
        top: 50%;
        transform: translateY(-50%);

        @media only screen and (max-width: 800px) {
            right: -10px;
        }
    }

    .prev {
        left: -35px;
        top: 50%;
        transform: translateY(-50%);

        @media only screen and (max-width: 800px) {
            left: -10px;
        }
    }
`

const Images = styled.div`
    .image {
        margin-right: 20px;
        outline: none;

        @media only screen and (max-width: 500px) {
            margin-right: 10px;
        }

        img {
            height: 350px;
            border: 1px solid #cccccc;
            width: auto;
            outline: none;
            background-color: white;

            @media only screen and (max-width: 800px) {
                height: 300px;
            }

            @media only screen and (max-width: 500px) {
                height: 280px;
            }

            @media only screen and (max-width: 400px) {
                height: 260px;
            }
        }
    }
`

const Info = styled.div`
    .name {
        font-size: 20px;
    }

    .price {
        font-size: 22px;
        font-weight: 500;
        margin-top: 5px;
    }

    .category {
        font-size: 14px;
        color: gray;
        margin-top: 7px;
    }

    .description {
        font-size: 14px;
        margin-top: 6px;
        white-space: pre-line;
    }
`

const NextArrow = ({ onClick }) => {
    return (
        <div className="arrow next" onClick={onClick}>
            <NextArrowSVG className="icon" />
        </div>
    )
}

const PrevArrow = ({ onClick }) => {
    return (
        <div className="arrow prev" onClick={onClick}>
            <PrevArrowSVG className="icon" />
        </div>
    )
}

const ProductInfo = ({ product, shop }) => {
    const hasImages = Array.isArray(product.images) && product.images.length > 0
    return (
        <View>
            {hasImages && (
                <Images style={{ marginBottom: product.images.length > 1 ? 32 : 10 }}>
                    {product.images.length > 1 ? (
                        <Slider
                            nextArrow={<NextArrow />}
                            prevArrow={<PrevArrow />}
                            infinite={false}
                            variableWidth
                            dots
                        >
                            {product.images.map((item) => (
                                <div className="image" key={item}>
                                    <img src={`${UploadsURL}/uploads/${item}`} alt={item} />
                                </div>
                            ))}
                        </Slider>
                    ) : (
                        <div className="image">
                            <img src={`${UploadsURL}/uploads/${product.images[0]}`} />
                        </div>
                    )}
                </Images>
            )}

            <Info>
                <div className="name">{product.name}</div>
                <div className="price">{convertPrice(product.price)} руб.</div>
                {product.category && <div className="category">{product.category.name}</div>}
                {product.subcategory && <div className="category">{product.subcategory.name}</div>}
                {product.description && <div className="description">{product.description}</div>}
            </Info>
        </View>
    )
}

export default ProductInfo
