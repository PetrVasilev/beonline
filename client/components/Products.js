import React, { useState, useEffect, useRef } from 'react'
import styled from 'styled-components'

import Spinner from './Spinner'
import Product from './Product'

const Categories = styled.div`
    display: flex;
    flex-direction: row;
    align-items: center;
    flex-wrap: wrap;
    padding-bottom: 15px;
    padding-top: 25px;
    border-bottom: 1px solid rgba(var(--b38, 219, 219, 219), 1);

    @media only screen and (max-width: 500px) {
        padding-bottom: 15px;
        padding-top: 15px;
        flex-wrap: nowrap;
        overflow-x: scroll;
        white-space: nowrap;
    }

    .category {
        padding: 5px 12px;
        border: 1px solid silver;
        font-size: 14px;
        border-radius: 5px;
        margin-right: 10px;
        margin-bottom: 10px;
        cursor: pointer;

        @media only screen and (max-width: 500px) {
            margin-bottom: 0;
            padding: 5px 10px;
        }
    }

    .active {
        background: #0070da;
        border: 1px solid #0070da;
        color: white;
    }
`

const ProductsView = styled.div`
    padding-top: 30px;

    @media only screen and (max-width: 500px) {
        padding-top: 20px;
    }

    .no-data {
        font-size: 14px;
        color: gray;
        text-align: center;
    }
`

const ProductsList = styled.div`
    display: grid;
    grid-template-columns: repeat(3, 1fr);
    grid-gap: 20px;

    @media only screen and (max-width: 710px) {
        grid-template-columns: repeat(2, 1fr);
    }

    @media only screen and (max-width: 500px) {
        grid-gap: 10px;
    }
`

const Loading = styled.div`
    width: 100%;
    display: flex;
    align-items: center;
    justify-content: center;
    height: 22vh;
`

const Products = ({ data: queryData, shop }) => {
    const { data, loading, refetch } = queryData

    const [category, setCategory] = useState(0)
    const [subcategory, setSubcategory] = useState(0)
    const [subcategories, setSubcategories] = useState([])

    const categories = data ? data.categories : []
    const products = data ? data.products : []

    const firstUpdate = useRef(true)

    useEffect(() => {
        if (firstUpdate.current) {
            firstUpdate.current = false
            return
        }
        refetch({
            categoryId: category,
            subcategoryId: subcategory
        })
        const filtered = categories.filter((item) => item.id === category)
        if (filtered[0]) {
            setSubcategories(
                filtered[0].subcategories.sort((a, b) =>
                    a.sortNumber > b.sortNumber ? 1 : a.sortNumber < b.sortNumber ? -1 : 0
                )
            )
        } else {
            setSubcategories([])
        }
    }, [category, subcategory])

    return (
        <>
            {categories.length > 0 && (
                <Categories>
                    <div
                        onClick={() => {
                            setCategory(0)
                            setSubcategory(0)
                        }}
                        className={`category${category === 0 ? ' active' : ''}`}
                    >
                        Все категории
                    </div>
                    {categories.map((item) => (
                        <div
                            onClick={() => setCategory(item.id)}
                            key={item.id}
                            className={`category${category === item.id ? ' active' : ''}`}
                        >
                            {item.name}
                        </div>
                    ))}
                </Categories>
            )}

            {subcategories.length > 0 && (
                <Categories>
                    <div
                        onClick={() => setSubcategory(0)}
                        className={`category${subcategory === 0 ? ' active' : ''}`}
                    >
                        Все подкатегории
                    </div>
                    {subcategories.map((item) => (
                        <div
                            onClick={() => setSubcategory(item.id)}
                            key={item.id}
                            className={`category${subcategory === item.id ? ' active' : ''}`}
                        >
                            {item.name}
                        </div>
                    ))}
                </Categories>
            )}

            <ProductsView>
                {loading ? (
                    <Loading>
                        <Spinner />
                    </Loading>
                ) : products.length > 0 ? (
                    <ProductsList>
                        {products.map((item) => (
                            <Product key={item.id} product={item} shop={shop} />
                        ))}
                    </ProductsList>
                ) : (
                    <div className="no-data">Товаров нет</div>
                )}
            </ProductsView>
        </>
    )
}

export default Products
