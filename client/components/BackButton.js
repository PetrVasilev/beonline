import React from 'react'
import styled from 'styled-components'
import Router from 'next/router'

import PrevArrowSVG from '../public/icons/prev-arrow.svg'

const View = styled.div`
    margin-bottom: 20px;
    padding: 5px 0;
    cursor: pointer;
    display: flex;
    flex-direction: row;
    align-items: center;
    width: 100px;

    svg {
        width: 20px;
        height: 20px;
    }

    .text {
        font-size: 16px;
        margin-left: 5px;
        color: gray;

        @media only screen and (max-width: 500px) {
            font-size: 14px;
        }
    }
`

const BackButton = () => {
    return (
        <View
            onClick={() => {
                Router.back()
            }}
        >
            <PrevArrowSVG />
            <div className="text">Назад</div>
        </View>
    )
}

export default BackButton
