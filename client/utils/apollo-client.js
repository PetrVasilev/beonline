import { ApolloClient } from 'apollo-client'
import { InMemoryCache } from 'apollo-cache-inmemory'
import { HttpLink } from 'apollo-link-http'
import { setContext } from 'apollo-link-context'
import { onError } from 'apollo-link-error'
import { ApolloLink } from 'apollo-link'
import fetch from 'isomorphic-unfetch'

// const host = 'http://192.168.0.44:3000'
const host = 'http://localhost:3000'

export default function createApolloClient(initialState) {
    const isServer = typeof window === 'undefined'

    const graphQLUrl =
        process.env.NODE_ENV !== 'production'
            ? `${host}/graphql`
            : isServer
            ? 'http://localhost:3000/graphql'
            : `/graphql`

    const httpLink = new HttpLink({
        uri: graphQLUrl,
        credentials: 'same-origin',
        fetch
    })

    const contextLink = setContext(() => {
        return {}
    })

    const errorLink = onError(({ graphQLErrors, networkError }) => {
        if (graphQLErrors)
            graphQLErrors.map(({ message }) => console.log(`[GraphQL error]: ${message}`))

        if (networkError) console.log(`[Network error]: ${networkError}`)
    })

    const link = ApolloLink.from([errorLink, contextLink, httpLink])

    return new ApolloClient({
        ssrMode: isServer,
        link,
        cache: new InMemoryCache().restore(initialState || {})
    })
}
