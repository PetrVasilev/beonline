export const UploadsURL =
    process.env.NODE_ENV === 'production' ? 'https://beonline.me' : 'http://localhost:3000'

export const phoneMask = (value) => {
    const removedChars = value.replace(/ /g, '').replace(/\D/g, '')
    const isNum = /^\d+$/.test(removedChars)
    if (!isNum) return null
    if (removedChars) {
        if (removedChars.length === 11) {
            if (removedChars.match(/^(\+7|7|8)+/g)) {
                let arr = removedChars
                    .replace(/^(\+7|7|8)+/g, '+7')
                    .match(/^(\+7|7|8)?(\d{1,3})?(\d{1,3})?(\d{1,2})?(\d{1,2})?/)
                let res = ''
                for (let i = 1; i < 6; i++) {
                    let item = arr[i]
                    if (item) {
                        if (i == 2) {
                            res = res + ' ' + `(${item}`
                        } else if (i == 3) {
                            res = res + ') ' + item
                        } else {
                            res = res + ' ' + item
                        }
                    }
                }
                return res.replace(/^ /, '')
            } else {
                return removedChars
            }
        } else {
            if (removedChars.length === 6) {
                const arr = removedChars.match(/.{1,2}/g)
                return arr.join('-')
            } else {
                return removedChars
            }
        }
    } else {
        return removedChars
    }
}

export const convertPrice = (price) => {
    if (price % 1 === 0) {
        return price
    } else {
        return price.toFixed(2)
    }
}
