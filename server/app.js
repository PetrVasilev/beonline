const express = require('express')
const dotenv = require('dotenv')
const { GraphQLServer } = require('graphql-yoga')
const { PrismaClient } = require('@prisma/client')

const resolvers = require('./resolvers')
const { checkAdmin, checkShop } = require('./utils/auth')
const { createAdmin } = require('./utils/admin')

dotenv.config()

const isProduction = process.env.NODE_ENV === 'production'
const port = process.env.PORT || 3000

const prisma = new PrismaClient({ errorFormat: isProduction ? 'minimal' : 'pretty' })

const server = new GraphQLServer({
    typeDefs: 'schemas/index.graphql',
    resolvers,
    context: ({ request }) => {
        const { authorization } = request.headers
        return {
            checkAdmin: () => {
                return checkAdmin(authorization)
            },
            checkShop: () => {
                return checkShop(authorization)
            },
            prisma
        }
    }
})

server.express.use((_, res, next) => {
    res.header('Access-Control-Allow-Origin', '*')
    res.header('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept')
    next()
})

server.express.use('/uploads', express.static('../uploads'))

server.start(
    {
        port,
        endpoint: '/graphql',
        playground: '/playground',
        bodyParserOptions: { limit: '10mb', type: 'application/json' },
        debug: isProduction ? false : true
    },
    () => {
        console.log(`Server is running on port:${port}`)
        createAdmin(prisma)
        if (!process.env.SHOP_DOMAIN_NAME) {
            throw new Error('SHOP_DOMAIN_NAME in .env file is required')
        }
    }
)
