const jwt = require('jsonwebtoken')
const bcrypt = require('bcryptjs')

const errors = require('../utils/errors')
const sendMail = require('../utils/mailer')
const { cryptPassword } = require('../utils/auth')
const { randomNumber } = require('../utils')
const { processUpload, deleteFile } = require('../utils/upload')

module.exports = {
    Query: {
        shop: async (_, {}, { checkShop, prisma }) => {
            const permission = checkShop()
            if (!permission) throw new Error(errors.notAccess)
            return await prisma.shop.findOne({
                where: {
                    id: permission.id
                }
            })
        },
        shops: async (_, {}, { checkAdmin, prisma }) => {
            const permission = await checkAdmin()
            if (!permission) throw new Error(errors.notAccess)
            return await prisma.shop.findMany({
                orderBy: {
                    createdAt: 'desc'
                }
            })
        },
        shopInfo: async (_, { where }, { prisma }) => {
            const { url } = where
            const shopByUrl = await prisma.shop.findOne({ where: { url } })
            if (!shopByUrl) {
                let id = parseInt(url)
                if (isNaN(id)) {
                    throw new Error(errors.notFound)
                }
                const shopById = await prisma.shop.findOne({ where: { id } })
                if (!shopById) throw new Error(errors.notFound)
                if (shopById.access === 0) throw new Error(errors.notFound)
                return shopById
            } else {
                if (shopByUrl.access === 0) throw new Error(errors.notFound)
                return shopByUrl
            }
        }
    },
    Mutation: {
        createShop: async (_, { data }, { checkAdmin, prisma }) => {
            const permission = await checkAdmin()
            if (!permission) throw new Error(errors.notAccess)
            const password = randomNumber(10000, 999999)
            const exist = await prisma.shop.findOne({
                where: {
                    email: data.email
                }
            })
            if (exist) throw new Error(errors.alreadyExist)
            sendMail({
                to: data.email,
                subject: 'Регистрация в сервисе BeOnline',
                text: 'Регистрация в сервисе BeOnline',
                html: `
                    <p>Здравствуйте, вы были зарегистрированы в сервисе BeOnline</p>
                    <p>Логин: ${data.email}<p>
                    <p>Пароль: ${password}<p>
                    <p>
                        <a href="${process.env.SHOP_DOMAIN_NAME}/login?after=password">Войти в личный кабинет</a>
                    </p>
                    <p>Измените пароль после входа</p>
                `
            })
            return await prisma.shop.create({
                data: {
                    ...data,
                    password: cryptPassword(password.toString())
                }
            })
        },
        deleteShop: async (_, { where }, { checkAdmin, prisma }) => {
            const permission = await checkAdmin()
            if (!permission) throw new Error(errors.notAccess)
            const deleting = await prisma.shop.findOne({ where })
            if (!deleting) throw new Error(errors.notFound)
            if (deleting.logo) {
                deleteFile(deleting.logo)
            }
            const allOrders = await prisma.order.findMany({
                where: {
                    shop: { id: deleting.id }
                }
            })
            await prisma.cartItem.deleteMany({
                where: {
                    order: { id: { in: allOrders.map((item) => item.id) } }
                }
            })
            const products = await prisma.product.findMany({
                where: {
                    shop: { id: deleting.id }
                }
            })
            products.forEach((item) => {
                if (Array.isArray(item.images)) {
                    item.images.forEach((image) => deleteFile(image))
                }
            })
            await Promise.all([
                prisma.product.deleteMany({
                    where: { shop: { id: deleting.id } }
                }),
                prisma.order.deleteMany({
                    where: { shop: { id: deleting.id } }
                })
            ])
            await prisma.category.deleteMany({
                where: { shop: { id: deleting.id } }
            })
            return await prisma.shop.delete({
                where
            })
        },
        setAccessShop: async (_, { where, data }, { checkAdmin, prisma }) => {
            const permission = await checkAdmin()
            if (!permission) throw new Error(errors.notAccess)
            return await prisma.shop.update({
                where,
                data
            })
        },
        loginShop: async (_, { data }, { prisma }) => {
            const { email, password } = data
            const shop = await prisma.shop.findOne({
                where: { email }
            })
            if (!shop) throw new Error(errors.notFound)
            const compareState = bcrypt.compareSync(password, shop.password)
            if (!compareState) throw new Error(errors.authError)
            return {
                token: jwt.sign({ id: shop.id }, process.env.SHOP_SECRET),
                shop
            }
        },
        editPasswordShop: async (_, { data }, { prisma, checkShop }) => {
            const permission = await checkShop()
            if (!permission) throw new Error(errors.notAccess)
            const shop = await prisma.shop.findOne({
                where: {
                    id: permission.id
                }
            })
            if (!shop) throw new Error(errors.notFound)
            const { currentPassword, newPassword } = data
            const compareState = bcrypt.compareSync(currentPassword, shop.password)
            if (!compareState) throw new Error(errors.authError)
            return await prisma.shop.update({
                data: {
                    password: cryptPassword(newPassword)
                },
                where: {
                    id: permission.id
                }
            })
        },
        editShop: async (_, { data, where }, { checkShop, prisma }) => {
            const permission = await checkShop()
            if (!permission) throw new Error(errors.notAccess)
            const { logo, url } = data
            if (logo) {
                if (!logo.uploaded) {
                    data.logo = await processUpload(logo, 'logo')
                } else {
                    data.logo = data.logo.name
                }
            } else {
                const shop = await prisma.shop.findOne({ where: { id: permission.id } })
                deleteFile(shop.logo)
                data.logo = null
            }
            if (url) {
                const exist = await prisma.shop.findOne({ where: { url } })
                if (exist) {
                    if (exist.id !== permission.id) {
                        throw new Error(errors.alreadyExist)
                    }
                }
            }
            return await prisma.shop.update({ data, where })
        }
    }
}
