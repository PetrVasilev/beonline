const admin = require('./admin')
const shop = require('./shop')
const category = require('./category')
const product = require('./product')
const order = require('./order')

module.exports = {
    Query: {
        ...admin.Query,
        ...shop.Query,
        ...category.Query,
        ...product.Query,
        ...order.Query
    },
    Mutation: {
        ...admin.Mutation,
        ...shop.Mutation,
        ...category.Mutation,
        ...product.Mutation,
        ...order.Mutation
    }
}
