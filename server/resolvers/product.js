const errors = require('../utils/errors')
const { processUpload, deleteFile } = require('../utils/upload')

module.exports = {
    Query: {
        product: async (_, { where }, { prisma }) => {
            return await prisma.product.findOne({
                where,
                include: { category: true, shop: true, subcategory: true }
            })
        },
        products: async (_, { where }, { prisma }) => {
            const { shopId, categoryId, subcategoryId, ids, archive } = where
            const prismaWhere = {
                shop: { id: shopId }
            }
            if (categoryId) {
                prismaWhere['category'] = { id: categoryId }
            }
            if (subcategoryId) {
                prismaWhere['subcategory'] = { id: subcategoryId }
            }
            if (Array.isArray(ids)) {
                prismaWhere['id'] = {
                    in: ids
                }
            }
            if (typeof archive !== 'undefined') {
                prismaWhere['archive'] = archive
            }
            return await prisma.product.findMany({
                where: prismaWhere,
                include: {
                    shop: true,
                    category: true,
                    subcategory: true
                },
                orderBy: {
                    sortNumber: 'asc'
                }
            })
        }
    },
    Mutation: {
        createProduct: async (_, { data }, { checkShop, prisma }) => {
            const permission = await checkShop()
            if (!permission) throw new Error(errors.notAccess)
            const { images, categoryId, subcategoryId } = data
            if (Array.isArray(images)) {
                const uploading = images.map(async (item) => {
                    return await processUpload(item)
                })
                data.images = {
                    set: await Promise.all(uploading)
                }
            } else {
                data.images = {
                    set: []
                }
            }
            delete data['categoryId']
            delete data['subcategoryId']
            const prismaData = {
                ...data,
                shop: {
                    connect: { id: permission.id }
                }
            }
            if (categoryId) {
                prismaData.category = { connect: { id: categoryId } }
            }
            if (subcategoryId) {
                prismaData.subcategory = { connect: { id: subcategoryId } }
            }
            return await prisma.product.create({
                data: prismaData,
                include: {
                    shop: true,
                    category: true,
                    subcategory: true
                }
            })
        },
        editProduct: async (_, { data, where }, { checkShop, prisma }) => {
            const permission = await checkShop()
            if (!permission) throw new Error(errors.notAccess)
            const { images, deletedImages, categoryId, subcategoryId } = data
            if (Array.isArray(deletedImages)) {
                deletedImages.forEach((image) => {
                    deleteFile(image)
                })
            }
            if (Array.isArray(images)) {
                const uploading = images.map(async (item) => {
                    if (item.uploaded) return item.name
                    return await processUpload(item)
                })
                data.images = {
                    set: await Promise.all(uploading)
                }
            }
            delete data['deletedImages']
            delete data['categoryId']
            delete data['subcategoryId']
            if (categoryId) {
                data['category'] = { connect: { id: categoryId } }
            }
            if (subcategoryId) {
                data['subcategory'] = { connect: { id: subcategoryId } }
            }
            return await prisma.product.update({
                where,
                data,
                include: { shop: true, category: true, subcategory: true }
            })
        },
        deleteProduct: async (_, { where }, { checkShop, prisma }) => {
            const permission = await checkShop()
            if (!permission) throw new Error(errors.notAccess)
            await prisma.cartItem.deleteMany({
                where: {
                    product: { id: where.id }
                }
            })
            const deleted = await prisma.product.delete({
                where,
                include: {
                    shop: true,
                    category: true,
                    subcategory: true
                }
            })
            if (Array.isArray(deleted.images)) {
                deleted.images.forEach((image) => deleteFile(image))
            }
            return deleted
        },
        setSortProduct: async (_, { data }, { prisma, checkShop }) => {
            const permission = await checkShop()
            if (!permission) throw new Error(errors.notAccess)
            for (let item of data) {
                await prisma.product.update({
                    where: {
                        id: item.productId
                    },
                    data: {
                        sortNumber: item.sortNumber
                    }
                })
            }
            return true
        }
    }
}
