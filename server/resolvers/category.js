const errors = require('../utils/errors')

module.exports = {
    Query: {
        categories: async (_, { where }, { prisma }) => {
            const { shopId } = where
            const categories = await prisma.category.findMany({
                where: {
                    shop: { id: shopId }
                },
                include: { shop: true, subcategories: true },
                orderBy: {
                    sortNumber: 'asc'
                }
            })
            return categories
        },
        category: async (_, { where }, { prisma }) => {
            return await prisma.category.findOne({
                where,
                include: { shop: true, subcategories: true }
            })
        }
    },
    Mutation: {
        createCategory: async (_, { data }, { prisma, checkShop }) => {
            const permission = await checkShop()
            if (!permission) throw new Error(errors.notAccess)
            return await prisma.category.create({
                data: {
                    ...data,
                    shop: {
                        connect: { id: permission.id }
                    }
                },
                include: {
                    shop: true,
                    subcategories: true
                }
            })
        },
        createSubcategory: async (_, { data, where }, { prisma, checkShop }) => {
            const permission = await checkShop()
            if (!permission) throw new Error(errors.notAccess)
            return await prisma.subcategory.create({
                data: {
                    ...data,
                    category: {
                        connect: { id: where.id }
                    }
                }
            })
        },
        deleteCategory: async (_, { where }, { checkShop, prisma }) => {
            const permission = await checkShop()
            if (!permission) throw new Error(errors.notAccess)
            return await prisma.category.delete({
                where,
                include: {
                    shop: true,
                    subcategories: true
                }
            })
        },
        deleteSubcategory: async (_, { where }, { checkShop, prisma }) => {
            const permission = await checkShop()
            if (!permission) throw new Error(errors.notAccess)
            return await prisma.subcategory.delete({
                where
            })
        },
        setSortCategory: async (_, { data }, { checkShop, prisma }) => {
            const permission = await checkShop()
            if (!permission) throw new Error(errors.notAccess)
            for (let item of data) {
                await prisma.category.update({
                    where: {
                        id: item.categoryId
                    },
                    data: {
                        sortNumber: item.sortNumber
                    }
                })
            }
            return true
        },
        setSortSubcategory: async (_, { data }, { checkShop, prisma }) => {
            const permission = await checkShop()
            if (!permission) throw new Error(errors.notAccess)
            for (let item of data) {
                await prisma.subcategory.update({
                    where: {
                        id: item.subcategoryId
                    },
                    data: {
                        sortNumber: item.sortNumber
                    }
                })
            }
            return true
        }
    }
}
