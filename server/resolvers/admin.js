const bcrypt = require('bcryptjs')
const jwt = require('jsonwebtoken')

const errors = require('../utils/errors')
const { cryptPassword } = require('../utils/auth')
const sendMail = require('../utils/mailer')

module.exports = {
    Query: {
        admin: async (_, {}, { prisma, checkAdmin }) => {
            const permission = await checkAdmin()
            if (!permission) throw new Error(errors.notAccess)
            try {
                const admin = await prisma.admin.findOne({
                    where: {
                        id: permission.id
                    }
                })
                return admin
            } catch (err) {
                throw new Error(errors.notAccess)
            }
        }
    },
    Mutation: {
        loginAdmin: async (_, { data }, { prisma }) => {
            const { login, password } = data
            const admin = await prisma.admin.findOne({
                where: {
                    login
                }
            })
            if (!admin) throw new Error(errors.notFound)
            const compareState = bcrypt.compareSync(password, admin.password)
            if (!compareState) throw new Error(errors.authError)
            return {
                token: jwt.sign({ id: admin.id }, process.env.ADMIN_SECRET),
                admin
            }
        },
        editPasswordAdmin: async (_, { data }, { prisma, checkAdmin }) => {
            const { currentPassword, newPassword } = data
            const permission = await checkAdmin()
            if (!permission) throw new Error(errors.notAccess)
            const admin = await prisma.admin.findOne({
                where: {
                    id: permission.id
                }
            })
            if (!admin) throw new Error(errors.notFound)
            const compareState = bcrypt.compareSync(currentPassword, admin.password)
            if (!compareState) throw new Error(errors.authError)
            return await prisma.admin.update({
                data: {
                    password: cryptPassword(newPassword)
                },
                where: {
                    id: permission.id
                }
            })
        },
        mailing: async (_, { data }, { prisma, checkAdmin }) => {
            const { title, description } = data
            const permission = await checkAdmin()
            if (!permission) throw new Error(errors.notAccess)
            const shops = await prisma.shop.findMany({
                select: {
                    email: true
                }
            })
            const emails = shops.map((item) => item.email)
            try {
                await sendMail({
                    to: emails,
                    subject: title,
                    text: title,
                    html: `
                        <p>${description.replace(/\n/g, '<br />')}</p>
                    `
                })
                return true
            } catch (err) {
                console.error(err)
                return false
            }
        }
    }
}
