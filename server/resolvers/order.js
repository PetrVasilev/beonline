const errors = require('../utils/errors')
const sendMail = require('../utils/mailer')
const { convertPrice } = require('../utils')

const getStatusTextOfOrder = (status) => {
    switch (status) {
        case 0:
            return 'Новый'
        case 1:
            return 'В процессе'
        case 2:
            return 'Выполнен'
        case 3:
            return 'Отменен'
        default:
            return ''
    }
}

module.exports = {
    Query: {
        orders: async (_, { where, skip, first, search }, { checkShop, prisma }) => {
            const permission = await checkShop()
            if (!permission) throw new Error(errors.notAccess)
            const generalWhere = {
                shop: { id: permission.id },
                ...where
            }
            if (search) {
                generalWhere['OR'] = [
                    {
                        userName: { contains: search }
                    },
                    {
                        userPhone: { contains: search }
                    },
                    {
                        userEmail: { contains: search }
                    },
                    {
                        userAddress: { contains: search }
                    }
                ]
            }
            return await prisma.order.findMany({
                where: generalWhere,
                include: {
                    cartItems: {
                        include: {
                            product: true
                        }
                    },
                    shop: true
                },
                orderBy: {
                    createdAt: 'desc'
                },
                skip,
                first
            })
        },
        ordersCount: async (_, { where, search }, { checkShop, prisma }) => {
            const permission = await checkShop()
            if (!permission) throw new Error(errors.notAccess)
            const generalWhere = {
                shop: { id: permission.id },
                ...where
            }
            if (search) {
                generalWhere['OR'] = [
                    {
                        userName: { contains: search }
                    },
                    {
                        userPhone: { contains: search }
                    },
                    {
                        userEmail: { contains: search }
                    },
                    {
                        userAddress: { contains: search }
                    }
                ]
            }
            return await prisma.order.count({
                where: generalWhere
            })
        },
        order: async (_, { where }, { checkShop, prisma }) => {
            const permission = await checkShop()
            if (!permission) throw new Error(errors.notAccess)
            return await prisma.order.findOne({
                where,
                include: {
                    cartItems: {
                        include: {
                            product: true
                        }
                    },
                    shop: true
                }
            })
        }
    },
    Mutation: {
        createOrder: async (_, { data }, { prisma }) => {
            const { cart, shopId, ...createData } = data

            // check for product exist
            const cartPromises = cart.map(async (item) => {
                const exist = await prisma.product.findOne({
                    where: { id: item.id }
                })
                if (exist) {
                    return item
                } else {
                    return null
                }
            })
            const mappedCart = await Promise.all(cartPromises)
            const existCart = mappedCart.filter((item) => item)

            const cartItems = {
                create: existCart.map((item) => {
                    return {
                        count: item.count,
                        product: {
                            connect: { id: item.id }
                        }
                    }
                })
            }
            const created = await prisma.order.create({
                data: {
                    ...createData,
                    shop: { connect: { id: shopId } },
                    cartItems
                },
                include: {
                    cartItems: {
                        include: {
                            product: true
                        }
                    },
                    shop: true
                }
            })
            let total = 0
            const shop = await prisma.shop.findOne({ where: { id: shopId } })
            const productsView = created.cartItems.map((item) => {
                const productSumm = item.count * item.product.price
                const shopUrl = shop.url ? shop.url : shop.id
                const productUrl = `${process.env.CLIENT_DOMAIN_NAME}/${shopUrl}/${item.product.id}`
                total += productSumm
                return `<li><a href="${productUrl}">${item.product.name}</a>, количество: ${item.count}, цена: ${convertPrice(productSumm)} руб.</li>`
            })
            const _text = `Новый заказ в магазине ${shop.name}`
            const _url = `
                <p>
                    <a href="${process.env.SHOP_DOMAIN_NAME}/orders/${created.id}">Посмотреть заказ в Beonline</a>
                </p>
            `
            const shopHtml = `
                <p>Заказ № ${created.id}</p>
                <p>Имя: ${createData.userName}</p>
                <p>Номер телефона: ${createData.userPhone}</p>
                ${createData.userAddress ? `<p>Адрес: ${createData.userAddress}</p>` : ''}
                ${createData.userEmail ? `<p>Email: ${createData.userEmail}</p>` : ''}
                <p>Сумма заказа: ${convertPrice(total)} руб.</p>
                ${_url}
            `
            sendMail({
                to: shop.email,
                subject: _text,
                text: _text,
                html: shopHtml
            })
            if (data.userEmail) {
                const text = `Ваш заказ в магазине ${shop.name}`
                const userHtml = `
                    <p>Здравствуйте, ${data.userName}</p>
                    <p>Ваш заказ № ${created.id} в магазине ${shop.name}</p>
                    <p>Товары:</p>
                    <ul>${productsView.join('')}</ul>
                    <p>Итого: ${convertPrice(total)} руб.</p>
                `
                setTimeout(() => {
                    sendMail({
                        to: data.userEmail,
                        subject: text,
                        text: text,
                        html: userHtml
                    })
                }, 5000)
            }
            return created
        },
        setStatusOrder: async (_, { where, data }, { checkShop, prisma }) => {
            const { id: orderId } = where
            const { status } = data
            const permission = await checkShop()
            if (!permission) throw new Error(errors.notAccess)
            const updated = await prisma.order.update({
                where: {
                    id: orderId
                },
                data: {
                    status
                },
                include: {
                    cartItems: {
                        include: {
                            product: true
                        }
                    },
                    shop: true
                }
            })
            if (updated.userEmail) {
                const text = `Статус вашего заказа в магазине ${updated.shop.name} изменен`
                const insideText = `Статус вашего заказа № ${updated.id} в магазине ${updated.shop.name} изменен:`
                sendMail({
                    to: updated.userEmail,
                    subject: text,
                    text: text,
                    html: `
                        <p>Здравствуйте, ${updated.userName}</p>
                        <p>${insideText}</p>
                        <p>Новый статус: ${getStatusTextOfOrder(updated.status)}</p>
                    `
                })
            }
            return updated
        },
        deleteOrder: async (_, { where }, { checkShop, prisma }) => {
            const permission = await checkShop()
            if (!permission) throw new Error(errors.notAccess)

            await prisma.cartItem.deleteMany({
                where: {
                    order: { id: where.id }
                }
            })

            return await prisma.order.delete({
                where,
                include: {
                    cartItems: {
                        include: {
                            product: true
                        }
                    },
                    shop: true
                }
            })
        }
    }
}
