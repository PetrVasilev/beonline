const nodemailer = require('nodemailer')

module.exports = ({ to, subject, text, html }) => {
    const mailconfig = {
        user: process.env.MAILER_EMAIL || '',
        password: process.env.MAILER_PASSWORD || ''
    }

    if (!mailconfig.user && !mailconfig.password) {
        console.log('EMAIL:', html)
        return null
    }

    const transporter = nodemailer.createTransport({
        host: 'smtp.yandex.ru',
        port: 465,
        secure: true,
        auth: {
            user: mailconfig.user,
            pass: mailconfig.password
        }
    })
    return new Promise((resolve, reject) => {
        const mailOptions = {
            from: mailconfig.user,
            to,
            subject,
            text,
            html
        }
        transporter.sendMail(mailOptions, (error) => {
            if (error) {
                console.log('[EMAIL ERROR]: ', error.message)
                return reject(false)
            }
            return resolve(true)
        })
    })
}
