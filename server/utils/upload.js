const fs = require('fs')
const shortid = require('shortid')
const sharp = require('sharp')

const destination = __dirname + '/../../uploads'

const deleteFile = (filename) => {
    const _path = `${destination}/${filename}`
    fs.unlink(_path, () => {})
}

const saveFile = async ({ stream, name }) => {
    const random = shortid.generate()
    const extension = name.split('.').pop()
    const filename = random + '.' + extension
    const path = `${destination}/${filename}`
    return new Promise((resolve, reject) =>
        stream
            .pipe(fs.createWriteStream(path))
            .on('finish', () => resolve(filename))
            .on('error', reject)
    )
}

const compressType = async ({ filename, type }) => {
    return new Promise((resolve) => {
        if (type) {
            const compressed = `${shortid.generate()}.png`
            sharp(`${destination}/${filename}`)
                .resize(500, 500)
                .png()
                .toFile(`${destination}/${compressed}`)
                .then(() => {
                    deleteFile(filename)
                    resolve(compressed)
                })
                .catch((err) => {
                    resolve(filename)
                })
        } else {
            resolve(filename)
        }
    })
}

const processUpload = async (upload, type) => {
    const { createReadStream, filename: name } = await upload
    const stream = createReadStream()
    const filename = await saveFile({ stream, name })
    if (type) {
        const compressed = await compressType({ filename, type })
        return compressed
    }
    return filename
}

module.exports = {
    processUpload,
    deleteFile,
    destination
}
