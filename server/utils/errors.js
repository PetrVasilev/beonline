module.exports = {
    notFound: 'not-found',
    notAccess: 'not-access',
    authError: 'auth-error',
    alreadyExist: 'already-exist',
    badRequest: 'bad-request',
    accessIsOver: 'access-is-over'
}
