function randomNumber(min = 0, max = 1000) {
    return Math.floor(Math.random() * (max - min) + min)
}

function convertPrice(price) {
    if (price % 1 === 0) {
        return price
    } else {
        return price.toFixed(2)
    }
}

module.exports = {
    randomNumber,
    convertPrice
}
