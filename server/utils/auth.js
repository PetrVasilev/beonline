const jwt = require('jsonwebtoken')
const bcrypt = require('bcryptjs')

const checkAdmin = (authorization) => {
    if (!authorization) {
        return null
    }
    return jwt.verify(authorization, process.env.ADMIN_SECRET, (err, decoded) => {
        if (err) {
            return null
        }
        return decoded
    })
}

const checkShop = (authorization) => {
    if (!authorization) {
        return null
    }
    return jwt.verify(authorization, process.env.SHOP_SECRET, (err, decoded) => {
        if (err) {
            return null
        }
        return decoded
    })
}

const cryptPassword = (password) => {
    if (!password) throw new Error('Password is required for crypt')
    const hash = bcrypt.hashSync(password)
    return hash
}

module.exports = {
    checkAdmin,
    checkShop,
    cryptPassword
}
