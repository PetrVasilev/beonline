const { cryptPassword } = require('./auth')

module.exports = {
    createAdmin: async (prisma) => {
        const login = process.env.ADMIN_LOGIN
        const password = process.env.ADMIN_PASSWORD

        if (!login) throw new Error('set property ADMIN_LOGIN in .env file')

        const admin = await prisma.admin.findOne({
            where: {
                login
            }
        })
        if (!admin) {
            try {
                await prisma.admin.create({
                    data: {
                        login,
                        password: cryptPassword(password)
                    }
                })
                console.log('Admin was created')
            } catch (err) {
                throw err
            }
        }
    }
}
